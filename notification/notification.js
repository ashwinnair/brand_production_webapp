var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Notification=new Schema({
        brand_id: {type: String,},
        retailer_id: {type: String},
        company_id: {type: String},
        user_id: {type: String},
        order: {type: String},
        isReadByAdmin: {type: Boolean, default: false},
        isReadByBrand: {type: Boolean, default: false},
        isReadByRetailer: {type: Boolean, default: false},
        isReadByUser: {type: Boolean, default: false},
        notificationAbout: {type: String, required: true},
        notificationMsg: {type: Object},
        initiatedBy: {type: String},
        notificationFor: {type: String},
        notificationScenario: {type: String},
        isForAdmin: {type: Boolean, default: false},
    },
    {
        timestamps: true
    });

module.exports = mongoose.model('Notification', Notification);
