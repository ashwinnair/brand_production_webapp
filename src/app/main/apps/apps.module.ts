import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NeedAuthGuard } from './auth.guard';
import { LightboxModule } from 'ngx-lightbox';
import { FuseSharedModule } from '@fuse/shared.module';
const routes = [
  {
    path: 'auth',
    loadChildren: './authentication/authentication.module#AuthenticationModule'
  },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule',
    canActivate: [NeedAuthGuard]
  },
  {
    path: 'b2b-administration',
    loadChildren: './b2b-administration/b2b-administration.module#B2BAdministrationModule',
    canActivate: [NeedAuthGuard]
  },
  {
    path: 'catalogue',
    loadChildren: './catalogue/catalogue.module#CatalogueModule',
    canActivate: [NeedAuthGuard]
  },
  {
    path: 'management',
    loadChildren: './management/management.module#ManagementModule',
    canActivate: [NeedAuthGuard]
  },
  {
    path: 'profile/:id',
    loadChildren: './profile/profile.module#ProfileModule',
    canActivate: [NeedAuthGuard]
  },
  {
    path: 'profile-retailer/:id',
    loadChildren: './profile-retailer/profile.module#ProfileModule',
    canActivate: [NeedAuthGuard]
  },
  {
    path: '**',
    loadChildren: './authentication/authentication.module#AuthenticationModule',
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    LightboxModule
  ],
  providers: [
    NeedAuthGuard
  ],
  declarations: []

 })
export class AppsModule {
}
