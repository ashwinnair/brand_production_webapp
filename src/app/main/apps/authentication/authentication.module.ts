import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule,MatCheckboxModule,
MatStepperModule,MatSelectModule
 } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { MailConfirmComponent } from './mail-confirm/mail-confirm.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { LockComponent } from './lock/lock.component';

const routes = [
   {
       path: 'register',
       component:RegisterComponent
   },
   {
       path: 'registration-form',
       component:RegistrationFormComponent
   },
   {
       path: 'login',
       component: LoginComponent
   },
   {
       path: 'forgot-password',
       component: ForgotPasswordComponent
   },
   {
       path: 'reset-password',
       component: ResetPasswordComponent
   },
   {
       path: 'mail-confirm',
       component :MailConfirmComponent
   },

   {
       path: 'lock',
       component:LockComponent
   },
   {
       path: '**',
       component: LoginComponent
   }

];

@NgModule({
  declarations: [
    MailConfirmComponent,
    ResetPasswordComponent,
    LoginComponent,
    ForgotPasswordComponent,
    RegisterComponent,
    RegistrationFormComponent,
    LockComponent
  ],
   imports: [
       RouterModule.forChild(routes),
       FuseSharedModule,
       MatButtonModule,
       MatFormFieldModule,
       MatIconModule,
       MatInputModule,
       MatCheckboxModule,
       MatStepperModule,
       MatSelectModule
   ],
})

export class AuthenticationModule
{

}
