export interface ForgotPasswordModel {
  token: string;
  error: string;
  user: any;
}
