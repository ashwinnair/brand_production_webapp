import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { ForgotPasswordModel} from './forgot-password.model';
import { CustomerService } from '../../customer.service';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(
    private http: HttpClient,
    private customer: CustomerService
  ) {
  }

  ForgotPassword(email: string): Observable<ForgotPasswordModel>{
    return this.http.post<ForgotPasswordModel>(this.customer.Baseurl + 'auth/forgot', {
      email: email
    });
  }
}