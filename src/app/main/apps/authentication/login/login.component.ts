import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginService } from './login.service';
import { CustomerService } from '../../customer.service';
import { Router } from '@angular/router';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;

    email = '';
    password = '';

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private api: LoginService,
        private customer: CustomerService,
        private router: Router
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.loginForm = this._formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    tryLogin() {

        this.api.login(
            this.loginForm.value.email,
            this.loginForm.value.password
        )
            .subscribe(
                response => {

                  this.afterlogin(response)
                });
    }

    afterlogin(response)
    {
      localStorage.setItem('userBrand', JSON.stringify(response.data.userDetails));
      this.customer.establishconnection(response.data.userDetails);
      if (response.data.userDetails.token) {
        this.redirectfn(response.data.userDetails);
      }
    }

    getUserDetails(data) {
        this.api.getUserData(data)
        .subscribe(
            response => {
                console.log(response);
                this.customer.setUserData(JSON.stringify(response));
            });
    }

    redirectfn(response){
        this.customer.setToken(response.token,response.userData);
        console.log(response.userData.status);
        if((response.userData.status ==  1) && (response.userData.emailverified == false) ){
          this.router.navigateByUrl('apps/auth/mail-confirm');
        }
        else if(response.userData.status ==  2){
          this.router.navigateByUrl('apps/dashboard');
        }
    }

    createaccount(){
      this.router.navigateByUrl('apps/auth/approval-pending');
    }

    forgotpassword(){
      this.router.navigateByUrl('apps/auth/forgot-password');
    }


}
