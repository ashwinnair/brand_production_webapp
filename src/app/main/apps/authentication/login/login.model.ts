export interface LoginModel {
  token: string;
  userDetails:any;
  error: string;
  emailverified:boolean;
  status: number;
}
