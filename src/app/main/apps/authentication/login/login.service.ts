import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginModel} from './login.model';
import { CustomerService } from '../../customer.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient,
    private customer: CustomerService
  ) {
  }

  login(email: string, password: string): Observable<LoginModel>{

    const body = new FormData();
    body.append('email',email);
    body.append('password',password);
    return this.http.post<LoginModel>(this.customer.AdminBaseurl + 'team/auth/login', body);

  }

  getUserData(data): Observable<any>{
    console.log(data);
    return this.http.get<any>(this.customer.Baseurl + 'users/getId/'+ data.userData._id + '/brand');
  }




}
