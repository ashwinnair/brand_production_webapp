import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { Router } from '@angular/router';
import { CustomerService } from '../../customer.service';
import {HttpClient} from '@angular/common/http';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
    selector     : 'mail-confirm',
    templateUrl  : './mail-confirm.component.html',
    styleUrls    : ['./mail-confirm.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class MailConfirmComponent implements OnInit
{

    verifyEmailForm: FormGroup;

    registrationDetails: any= {};
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     */
    constructor(
        private _formBuilder: FormBuilder,
        private _fuseConfigService: FuseConfigService,
        private customer: CustomerService,
        private router: Router,
        private http: HttpClient,
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.verifyEmailForm = this._formBuilder.group({
            VerifyToken: ['', [Validators.required]]
        });
        this.registrationDetails = JSON.parse(localStorage.getItem('userBrand'));
    }

    tryVerifyEmail() {

        console.log(this.customer.userRegistrationData);

        const body = new FormData();
        body.append('VerifyToken',this.verifyEmailForm.get('VerifyToken').value)

        this.http.post<any>(this.customer.Baseurl + 'team/auth/checkmailtoken/' + this.registrationDetails.userData._id, body)
        .subscribe(
            response=>{
                if (response.verified == true) {
                    this.router.navigateByUrl('apps/auth/login');
                }
           },
           error=>{
            console.log(error);
           }
        );
    }

}
