export interface RegisterModel {
  token: string;
  error: string;
  message: string;
}
