import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RegisterModel} from './register.model';
import { CustomerService } from '../../customer.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private http: HttpClient,
    private customer: CustomerService
    ) {
  }

  register(UserRegister): Observable<RegisterModel>{

    return this.http.post<RegisterModel>(this.customer.Baseurl + 'brand/auth/register', {
      fname: UserRegister.fname,
      lname: UserRegister.lname,
      email: UserRegister.email,
      mobileNo: UserRegister.mobileNo,
      landlineNo: UserRegister.landlineNo,
      designation: UserRegister.designation,
      companyName: UserRegister.companyName,
      brandName: UserRegister.brandName,
      password: UserRegister.password,
    });
  }
}
