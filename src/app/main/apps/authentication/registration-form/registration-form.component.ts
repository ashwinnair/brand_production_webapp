import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';
import { Subject } from 'rxjs';

import { RegisterFormService } from './registration-form.service';
import { CustomerService } from '../../customer.service';
import { Router } from '@angular/router';

import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit, OnDestroy {

    form: FormGroup;
    data:any;
    data1:any;
    GetID: any;

    horizontalStepperStep1: FormGroup;
    profiledetails: FormGroup;
    storeDetails: FormGroup;

    registeredaddressproofname:any = null;
    certofincname:any = null;
    br:any = null;
    panname:any = null;
    gstfile:any = null;



    private _unsubscribeAll: Subject<any>;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private api: RegisterFormService,
        private customer: CustomerService,
        private router: Router
    )
    {

      this._fuseConfigService.config = {
        layout: {
          navbar: {
            hidden: true
          },
          toolbar: {
            hidden: true
          },
          footer: {
            hidden: true
          },
          sidepanel: {
            hidden: true
          }
        }
      };
      this._unsubscribeAll = new Subject();

      this.horizontalStepperStep1 = _formBuilder.group({
          'firstname':['',Validators.required],
          'middlename':[''],
          'lastname':['',Validators.required],
          'designation':['',Validators.required],
          'email':['',Validators.required],
          'mobile':['',Validators.required],
          'landline':['',Validators.required]
      });
      this.profiledetails = _formBuilder.group({
            'companyname':[{value:'',disabled:true},Validators.required],
            'brandname':['',Validators.required],
            'address':['',Validators.required],
            'pincode':['',Validators.required],
            'state':['',Validators.required],
            'city':['',Validators.required],
            'mobile':['',Validators.required],
            'landline':[''],
            'registeredaddressproofname':[{value: '' , disabled:true}],
            'registeredaddressproof':['',Validators.required],
            'constitution':['',Validators.required],
            'certofincname':[{value: '' , disabled:true}],
            'certofinc':['',Validators.required],
            'BRname':[{value: '' , disabled:true}],
            'BR':[''],
            'panval':['',Validators.required],
            'panname':['',Validators.required],
            'GSTinName':['',Validators.required],
            'GST':['',Validators.required],
            'cin':['']
      });
      this.storeDetails = _formBuilder.group({
            'total_stores':['',Validators.required],
            'MRP_range':['',Validators.required],
            'topretailer1':['',Validators.required],
            'topretailer2':[''],
            'topretailer3':[''],
            'annualsalesvalue':['',Validators.required],
            'TopSegments1':['',Validators.required],
            'TopSegments2':[''],
            'TopSegments3':[''],
            'TopVertical1':['',Validators.required],
            'TopVertical2':[''],
            'TopVertical3':[''],
            'TopDepartment1':['',Validators.required],
            'TopDepartment2':[''],
            'TopDepartment3':[''],
      });

    }

    ngOnInit(): void {
        this.data1 = JSON.parse(localStorage.getItem('BrandInfo'));
        console.log("DATA",this.data1);
    }


    addpersonaldetails(){
      var inputformdata = this.horizontalStepperStep1.value;
      this.api.newregpersonaldetails(this.data1,inputformdata).subscribe(response=>{
      });
      console.log(this.data1);
      this.profiledetails.patchValue({
        'companyname':[this.data1.companyName],
        'brandname':[this.data1.brandName]
      })
    }

    regcompanydetails(){
      var inputformdata = this.profiledetails.value;
      this.api.newregcompanydetails(this.data1,inputformdata,this.registeredaddressproofname, this.certofincname,this.br,this.panname,this.gstfile).subscribe(response=>{
        console.log(response);
      });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    uploadrap(event){
        if(event.target.files.length > 0) {
          let file = event.target.files[0];
          console.log(file);
          this.profiledetails.get('registeredaddressproofname').setValue(file.name);
          this.registeredaddressproofname = file;

        }
    }

      uploadinc(event){
        if(event.target.files.length > 0){
          let file = event.target.files[0];
          this.profiledetails.get('certofincname').setValue(file.name);
          this.certofincname = file;
        }
      }
      uploadbr(event){
        if(event.target.files.length > 0){
          let file = event.target.files[0];
          this.profiledetails.get('BRname').setValue(file.name);
          this.br = file;
        }
      }
      uploadpan(event){
        if(event.target.files.length > 0){
          let file = event.target.files[0];
          this.panname = file;
        }
      }
      uploadgst(event){
        if(event.target.files.length > 0){
          let file = event.target.files[0];
          this.gstfile = file;
        }
      }

      regretailerstore(){
        var inputformdata = this.storeDetails.value;
        this.api.registerStoreDetails(this.data1,inputformdata).subscribe(response=>{
          this.updatestatus(response);
        });
      }

      updatestatus(response){
        if(response.status == 1)
        {
          this.api.pendingapproval(this.data1).subscribe(response =>{
            console.log("Account is Pending Activations");
              this.router.navigateByUrl('apps/auth/approval-pending');

          })
        }
      }


}
