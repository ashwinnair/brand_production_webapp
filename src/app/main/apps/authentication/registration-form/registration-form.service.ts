import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { RegisterFormModel } from './registration-form.model';
import { CustomerService } from '../../customer.service';



@Injectable({
  providedIn: 'root'
})

export class RegisterFormService {

  constructor(
    private http: HttpClient,
    private customer: CustomerService
  ) {
  }

  registerCompanyDetails(companyDetails, dataId, file): Observable<RegisterFormModel> {
    console.log(companyDetails);
    console.log(file);

    var body = {
      company_details: {
        company_name: companyDetails.company_details.companyName,
        retail_chain_name: companyDetails.company_details.retailChainName,
        brand_code: companyDetails.company_details.address,
        registered_address: companyDetails.company_details.address,
        City: companyDetails.company_details.city,
        State: companyDetails.company_details.state,
        pincode: companyDetails.company_details.postalCode,
        mobile: companyDetails.company_details.companyMobileNo,
        landline: companyDetails.company_details.companyLandlineNo,
        signing_authority_name: companyDetails.company_details.companySigningAuthName,
        signing_authority_designation: companyDetails.company_details.companySigningAuthDesignation,
        registered_address_proof: companyDetails.company_details.certificateIncorporation,
        address_proof: "file",
        constitution: companyDetails.company_details.constitution,
        firm_registration_certi: companyDetails.company_details.certificateIncorporation,
        certificate: companyDetails.company_details.certificateIncorporation,
        power_of_attorney: companyDetails.company_details.certificateIncorporation,
        attorney: companyDetails.company_details.certificateIncorporation,
        income_tax: companyDetails.company_details.IncomeTaxPan,
        tax: companyDetails.company_details.IncomeTaxPan,
        GST: companyDetails.company_details.GSTINUIN,
        GST_file: companyDetails.company_details.GSTINUIN,
        CIN: companyDetails.company_details.CIN
      },

      Corporate_office: {
        address: companyDetails.Corporate_office.CorporateOfficeAddress,
        city: companyDetails.Corporate_office.CorporateOfficeCity,
        state: companyDetails.Corporate_office.CorporateOfficeState,
        pincode: companyDetails.Corporate_office.CorporateOfficePostalCode,
        mobile: companyDetails.Corporate_office.CorporateOfficeMobileNo,
        landline: companyDetails.Corporate_office.CorporateOfficeLandlineNo,
        address_proof: companyDetails.Corporate_office.CorporateOfficeAddressProof,
        address_proof_file: companyDetails.Corporate_office.CorporateOfficeAddressProof,
        GST: companyDetails.Corporate_office.CorporateOfficeGSTINUIN,
        GST_file: companyDetails.Corporate_office.CorporateOfficeGSTINUIN
      },

      Ware_house_delivery: companyDetails.Warehouse_house_delivery,
    }

    const uploadData = new FormData();

    uploadData.append('data', JSON.stringify(body));

    for(var i = 0; i < file.length; i++){
      console.log(file[i]);
      uploadData.append('filename'+[i], file[i]);
    }
    return this.http.patch<RegisterFormModel>(this.customer.Baseurl + 'brand/registration/companydetails/' + dataId, uploadData);


  }

  registerStoreDetails(dataId,storeDetails): Observable<RegisterFormModel> {

    console.log(storeDetails);

    var body = {
      Brand_details: {
        total_stores: storeDetails.total_stores,
        top_retailers: [storeDetails.topretailer1,storeDetails.topretailer2,storeDetails.topretailer3],
        annual_sales: storeDetails.annualsalesvalue
      },
      segments: [storeDetails.TopSegments1, storeDetails.TopSegments2, storeDetails.TopSegments3],
      verticals: [storeDetails.TopVertical1, storeDetails.TopVertical2, storeDetails.TopVertical3],
      departments: [storeDetails.TopDepartment1, storeDetails.TopDepartment2, storeDetails.TopDepartment3]
    }
    const uploadData = new FormData();
    uploadData.append('data', JSON.stringify(body));

    return this.http.patch<RegisterFormModel>(this.customer.Baseurl + 'brand/registration/branddetails/' + dataId._id, uploadData)
  }


  newregpersonaldetails(data,inputdata):Observable<any>{
    console.log(data,inputdata);
    const body = {
      userId: data._id,
      status: data.status,
      authorised_signatory_details: inputdata,
      company_details: {
        company_name: data.companyName,
        retail_chain_name: null,
        retailer_code: null,
        registered_address: null,
        City: null,
        State: null,
        pincode: null,
        mobile: null,
        landline: null,
        signing_authority_name: null,
        signing_authority_designation: null,
        registered_address_proof: null,
        address_proof:null,
        constitution: null,
        firm_registration_certi: null,
        certificate: null,
        power_of_attorney: null,
        attorney: null,
        income_tax:null,
        tax: null,
        GST: null,
        GST_file: null,
        CIN: null
      },

      Corporate_office: {
        address: null,
        city: null,
        state: null,
        pincode: null,
        mobile: null,
        landline: null,
        address_proof: null,
        address_proof_file: null,
        GST: null,
        GST_file: null,
      },

      Ware_house_delivery: null,

      Brand_details: {
        total_stores: null,
        annual_sales: null,
        MRP_range: null,
        top_retatiler: [null,null,null],
      },
      segments: [null, null,null],
      verticals: [null, null, null],
      departments:  [null, null, null]


    }
    var uploadData = new FormData();
    uploadData.append('data',JSON.stringify(body));
    return this.http.post(this.customer.Baseurl + 'brand/registration/personal', uploadData);
  }

  newregcompanydetails(data,inputdata,registeredaddressproofname, certofincname,br,panname,gstfile):Observable<any>{

        console.log(data, inputdata,registeredaddressproofname, certofincname,br,panname,gstfile);

        var body = {
          company_details: {
            company_name: data.companyName ,
            brandname: inputdata.retailChainName,
            brand_code: null,
            registered_address: inputdata.address,
            City: inputdata.city,
            State: inputdata.state,
            pincode: inputdata.pincode,
            mobile: inputdata.mobile,
            landline: inputdata.landline,
            signing_authority_name: null,
            signing_authority_designation: null,
            registered_address_proof: null,
            address_proof: "file",
            constitution: inputdata.constitution,
            firm_registration_certi: null,
            certificate: null,
            power_of_attorney: null,
            attorney: null,
            income_tax: inputdata.panval,
            tax: null,
            GST: inputdata.GSTinName,
            GST_file: null,
            CIN: inputdata.cin
          },

          Corporate_office: {
            address: null,
            city:null,
            state: null,
            pincode: null,
            mobile: null,
            landline: null,
            address_proof: null,
            address_proof_file:null,
            GST: null,
            GST_file: null
          },

          Ware_house_delivery: null,
        }

        const uploadData = new FormData();

        uploadData.append('data', JSON.stringify(body));
        uploadData.append('filename0',registeredaddressproofname);
        uploadData.append('filename1',certofincname);
        uploadData.append('filename2',br);
        uploadData.append('filename3',panname);
        uploadData.append('filename4',gstfile);

        return this.http.patch<RegisterFormModel>(this.customer.Baseurl + 'brand/registration/companydetails/' + data._id, uploadData);



  }

  pendingapproval(data): Observable<RegisterFormModel>{
    const body = {
      status : 4
    }
    return this.http.patch<RegisterFormModel>(this.customer.Baseurl + 'retailer/auth/updateStatus/' + data._id, body);

  }




}
