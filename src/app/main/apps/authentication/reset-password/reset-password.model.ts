export interface ResetPasswordModel {
  token: string;
  error: string;
}