import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { ResetPasswordModel} from './reset-password.model';
import { CustomerService } from '../../customer.service';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  constructor(
    private http: HttpClient,
    private customer: CustomerService
  ) {
  }

  ResetPassword(token: string, password: string): Observable<ResetPasswordModel>{
    return this.http.put<ResetPasswordModel>(this.customer.Baseurl + 'auth/account/password', {
      token: token,
      password: password
    });
  }
}