import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatListModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatDialogModule

} from '@angular/material';

import { SidebarComponent } from './retailers/sidebar/sidebar.component';

import { DataTablesModule } from 'angular-datatables';

import { BrandsComponent } from './brands/brands.component';
import { RetailersComponent } from './retailers/retailers.component';
import { OrdersComponent } from './orders/orders.component';
import { PaymentComponent } from './payment/payment.component';
import { ReportComponent } from './report/report.component';
import { RetailerPaymentsComponent } from './payment/retailer-payments/retailer-payments.component';
import { DistributorPaymentsComponent } from './payment/distributor-payments/distributor-payments.component';
import { BrandPaymentComponent } from './payment/brand-payment/brand-payment.component';
import { PendingComponent } from './orders/pending/pending.component';
import { ApprovedComponent } from './orders/approved/approved.component';
import { ShippedComponent } from './orders/shipped/shipped.component';
import { CompleteComponent } from './orders/complete/complete.component';
import { RevisedComponent } from './orders/revised/revised.component';
import { OrderDetailsComponent } from './orders/order-details/order-details.component';
import { OrderDetailsEditComponent } from './orders/order-details-edit/order-details-edit.component';
import { PurchaseOrderComponent } from './orders/purchase-order/purchase-order.component';
import { OrderapproveComponent } from './orders/order-details/orderapprove/orderapprove.component';
import { ShipnowComponent } from './orders/order-details/shipnow/shipnow.component';
import { RsorApprovedComponent } from './orders/rsor-approved/rsor-approved.component';
import { RsorCompleteComponent } from './orders/rsor-complete/rsor-complete.component';
import { RsorPendingComponent } from './orders/rsor-pending/rsor-pending.component';
import { RsorRevisedComponent } from './orders/rsor-revised/rsor-revised.component';
import { RsorShippedComponent } from './orders/rsor-shipped/rsor-shipped.component';
import { DorApprovedComponent } from './orders/dor-approved/dor-approved.component';
import { DorCompleteComponent } from './orders/dor-complete/dor-complete.component';
import { DorPendingComponent } from './orders/dor-pending/dor-pending.component';
import { DorRevisedComponent } from './orders/dor-revised/dor-revised.component';
import { DorShippedComponent } from './orders/dor-shipped/dor-shipped.component';
import { DsorApprovedComponent } from './orders/dsor-approved/dsor-approved.component';
import { DsorCompleteComponent } from './orders/dsor-complete/dsor-complete.component';
import { DsorPendingComponent } from './orders/dsor-pending/dsor-pending.component';
import { DsorRevisedComponent } from './orders/dsor-revised/dsor-revised.component';
import { DsorShippedComponent } from './orders/dsor-shipped/dsor-shipped.component';
import { DistributorOrderDetailsComponent } from './orders/distributor-order-details/distributor-order-details.component';
import { DistributorPurchaseOrderComponent } from './orders/distributor-purchase-order/distributor-purchase-order.component';
import { DistributorOrderApproveComponent } from './orders/distributor-order-details/distributor-order-approve/distributor-order-approve.component';

const routes: Routes = [
    {
        path: 'brands',
        component: BrandsComponent
    },
    {
        path: 'retailers',
        component: RetailersComponent
    },
    {
        path: 'orders',
        component: OrdersComponent
    },
    {
        path: 'orders/details/:orderid/:brandid/:retailerid',
        component: OrderDetailsComponent
    },
    {
        path: 'purchase-order/:orderid',
        component: PurchaseOrderComponent
    },
    {
        path: 'distributororders/details/:orderid/:brandid/:distributorid',
        component: DistributorOrderDetailsComponent
    },
    {
        path: 'distributorpurchase-order/:orderid',
        component: DistributorPurchaseOrderComponent
    },
    {
        path: 'payment',
        component: PaymentComponent
    },
    {
        path: 'report',
        component: ReportComponent
    },
];

@NgModule({
    declarations: [
      BrandsComponent,
      RetailersComponent,
      OrdersComponent,
      PaymentComponent,
      ReportComponent,
      SidebarComponent,
      RetailerPaymentsComponent,
      DistributorPaymentsComponent,
      BrandPaymentComponent,
      PendingComponent,
      ApprovedComponent,
      ShippedComponent,
      CompleteComponent,
      RevisedComponent,
      OrderDetailsComponent,
      OrderDetailsEditComponent,
      PurchaseOrderComponent,
      OrderapproveComponent,
      ShipnowComponent,
      RsorApprovedComponent,
      RsorCompleteComponent,
      RsorPendingComponent,
      RsorRevisedComponent,
      RsorShippedComponent,
      DorApprovedComponent,
      DorCompleteComponent,
      DorPendingComponent,
      DorRevisedComponent,
      DorShippedComponent,
      DsorApprovedComponent,
      DsorCompleteComponent,
      DsorPendingComponent,
      DsorRevisedComponent,
      DsorShippedComponent,
      DistributorOrderDetailsComponent,
      DistributorPurchaseOrderComponent,
      DistributorOrderApproveComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        NgxSmartModalModule.forRoot(),
        TranslateModule,

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatListModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatCheckboxModule,
        MatRadioModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatDialogModule,
        FuseSharedModule,
        FuseSidebarModule,
        DataTablesModule
    ]
})

export class B2BAdministrationModule {
}
