import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerService } from '../../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss']
})
export class BrandsComponent implements OnInit {

  allBrands: any;
  data:any;

  constructor(
    private http: HttpClient,
    private customer: CustomerService,
    public router: Router
  ) {
  }

  ngOnInit() {

    this.data = JSON.parse(localStorage.getItem('BrandInfo'));
    console.log(this.data);
    this.http.get(this.customer.AdminBaseurl + 'brand/brandsByCompany/'+this.data.companyId)
      .subscribe(
        response => {
          this.setdata(response);
        });

  }

  setdata(response){
    this.allBrands = response.data;
  }
  gotoprofile(brand){
    let url = '/apps/profile/' + brand._id;
    this.router.navigateByUrl(url);
  }

  gotocatalogue(brand){
    let url = '/apps/catalogue/list/' +  brand.vendor_code;
    this.router.navigateByUrl(url);
  }


}
