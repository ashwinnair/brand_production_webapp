import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributorOrderApproveComponent } from './distributor-order-approve.component';

describe('DistributorOrderApproveComponent', () => {
  let component: DistributorOrderApproveComponent;
  let fixture: ComponentFixture<DistributorOrderApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributorOrderApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributorOrderApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
