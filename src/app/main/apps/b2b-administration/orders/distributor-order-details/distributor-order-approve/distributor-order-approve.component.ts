import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../orders.service';
import { Router,ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-distributor-order-approve',
  templateUrl: './distributor-order-approve.component.html',
  styleUrls: ['./distributor-order-approve.component.scss']
})
export class DistributorOrderApproveComponent implements OnInit {

  orderDetailsData: any;
  orderid:any;
  id:any;
  form: FormGroup;

  constructor(
      private api: OrdersService,
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      public ngxSmartModalService: NgxSmartModalService
      ) {

        const id = JSON.parse(localStorage.getItem('userBrand'));
        this.id = id.userData.brandId;
        let orderid = this.route.snapshot.params['orderid'];
        this.orderid = orderid;

        this.form = fb.group({
          'expectedShipmentDate':[''],
          'expectedDeliveryDate' :['']
        })

  }

  ngOnInit() {
  }
  submitOrderDate() {

    const body =
      {
          orderId: this.orderid,
          approvedBy: "brand",
          expectedShipmentDate: this.form.get('expectedShipmentDate').value ,
          expectedDeliveryDate : this.form.get('expectedDeliveryDate').value
      }
    this.api.approveDistributorOrder(body)
      .subscribe(
        response => {
          if(response.status) {
            this.router.navigateByUrl('apps/b2b-administration/orders');
          }
        },
        response => {
          alert(response.error.error);
        });
  }

}
