import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributorOrderDetailsComponent } from './distributor-order-details.component';

describe('DistributorOrderDetailsComponent', () => {
  let component: DistributorOrderDetailsComponent;
  let fixture: ComponentFixture<DistributorOrderDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributorOrderDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributorOrderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
