import { Component, OnInit, Inject } from '@angular/core';

import { OrdersService } from '../orders.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as jsPDF from 'jspdf';
import * as _ from 'lodash';

@Component({
  selector: 'app-distributor-purchase-order',
  templateUrl: './distributor-purchase-order.component.html',
  styleUrls: ['./distributor-purchase-order.component.scss']
})
export class DistributorPurchaseOrderComponent implements OnInit {

  orderid:any;
  purchasesummary:any;
  newValidDate:Date;

constructor(public api:OrdersService,
    private router: Router,
    private route: ActivatedRoute){

  let orderid = this.route.snapshot.params['orderid'];
  this.orderid = orderid;

}

 ngOnInit(){

   this.api.getdistributorpurchaseorderdetails(this.orderid).subscribe(response=>{

      this.purchasesummary = response.data.orderDetails;
      var approvedDate = this.purchasesummary.approvedDate;
      var date = new Date(approvedDate);
      console.log(date);
      var validDate = date.setDate(date.getDate() + 30);
      this.newValidDate = new Date(validDate);
      console.log(this.newValidDate);

      console.log(this.purchasesummary);
   })



 }

 generatePdf(){
   window.print();
 }


}
