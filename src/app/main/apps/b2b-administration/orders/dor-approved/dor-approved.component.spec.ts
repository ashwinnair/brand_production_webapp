import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DorApprovedComponent } from './dor-approved.component';

describe('DorApprovedComponent', () => {
  let component: DorApprovedComponent;
  let fixture: ComponentFixture<DorApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DorApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DorApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
