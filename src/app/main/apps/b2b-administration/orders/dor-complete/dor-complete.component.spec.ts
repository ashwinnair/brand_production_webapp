import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DorCompleteComponent } from './dor-complete.component';

describe('DorCompleteComponent', () => {
  let component: DorCompleteComponent;
  let fixture: ComponentFixture<DorCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DorCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DorCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
