import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { OrdersService } from '../orders.service';
import { Router } from '@angular/router';
import { CustomerService } from '../../../customer.service';
import { ExcelService } from '../../../../../services/excel.service';



@Component({
  selector: 'app-dor-complete',
  templateUrl: './dor-complete.component.html',
  styleUrls: ['./dor-complete.component.scss']
})
export class DorCompleteComponent implements OnInit {
  // Must be declared as "any", not as "DataTables.Settings"
  datas:any = [];
  id:any;
  dataArrayEmpty = true;

  constructor(
    private api: OrdersService,
    private router: Router,
    private customer: CustomerService,
    private http:HttpClient,
    private excelService:ExcelService
  ) {

    const id = JSON.parse(localStorage.getItem('userBrand'));
    this.id = id.userData.companyId;
  }

  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.datas, 'linsheet');
  }


  ngOnInit() {

    this.http.get(this.customer.AdminBaseurl + 'distributorOrder/brandOrderListing/' + this.id + '/complete/OR').subscribe(
      response => {
        this.setdata(response);
      }
    )
  }

  setdata(response){
    this.datas = response.data;

    if(this.datas.length > 0){
      this.dataArrayEmpty = false;
    }
  }


  someClickHandler(data) {
    
    this.api.getOrderID(data._id)
    this.router.navigateByUrl('/apps/b2b-administration/distributororders/details/' + data._id + '/' + data.brand_id._id + '/' + data.distributor_id._id);

  }

}
