import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DorPendingComponent } from './dor-pending.component';

describe('DorPendingComponent', () => {
  let component: DorPendingComponent;
  let fixture: ComponentFixture<DorPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DorPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DorPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
