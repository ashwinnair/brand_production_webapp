import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DorRevisedComponent } from './dor-revised.component';

describe('DorRevisedComponent', () => {
  let component: DorRevisedComponent;
  let fixture: ComponentFixture<DorRevisedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DorRevisedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DorRevisedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
