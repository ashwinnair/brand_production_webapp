import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DorShippedComponent } from './dor-shipped.component';

describe('DorShippedComponent', () => {
  let component: DorShippedComponent;
  let fixture: ComponentFixture<DorShippedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DorShippedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DorShippedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
