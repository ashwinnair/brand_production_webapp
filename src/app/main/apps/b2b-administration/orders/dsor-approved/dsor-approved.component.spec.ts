import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsorApprovedComponent } from './dsor-approved.component';

describe('DsorApprovedComponent', () => {
  let component: DsorApprovedComponent;
  let fixture: ComponentFixture<DsorApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsorApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsorApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
