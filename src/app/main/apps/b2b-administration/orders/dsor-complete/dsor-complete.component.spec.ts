import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsorCompleteComponent } from './dsor-complete.component';

describe('DsorCompleteComponent', () => {
  let component: DsorCompleteComponent;
  let fixture: ComponentFixture<DsorCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsorCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsorCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
