import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsorPendingComponent } from './dsor-pending.component';

describe('DsorPendingComponent', () => {
  let component: DsorPendingComponent;
  let fixture: ComponentFixture<DsorPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsorPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsorPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
