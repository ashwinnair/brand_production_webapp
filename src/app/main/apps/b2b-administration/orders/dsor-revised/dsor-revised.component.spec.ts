import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsorRevisedComponent } from './dsor-revised.component';

describe('DsorRevisedComponent', () => {
  let component: DsorRevisedComponent;
  let fixture: ComponentFixture<DsorRevisedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsorRevisedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsorRevisedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
