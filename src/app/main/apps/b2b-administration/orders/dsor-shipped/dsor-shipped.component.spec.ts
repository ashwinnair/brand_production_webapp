import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsorShippedComponent } from './dsor-shipped.component';

describe('DsorShippedComponent', () => {
  let component: DsorShippedComponent;
  let fixture: ComponentFixture<DsorShippedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsorShippedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsorShippedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
