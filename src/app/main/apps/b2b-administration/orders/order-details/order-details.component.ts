import { Component, OnInit,Inject } from '@angular/core';
import { OrdersService } from '../orders.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { CustomerService } from '../../../customer.service';
import { ExcelService } from '../../../../../services/excel.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})



export class OrderDetailsComponent implements OnInit {


  panelOpenState = false;
  readonlyInput: boolean = true;
  onEdit: boolean = false;
  approvedSelected: boolean = false;
  shipnowdiv:any = 'close';
  orderstatus:any;
  orderDetailsData: any = {};
  productData: any = {};
  productImage: any = [];
  receiptform:FormGroup;
  dispatchTerms:any;
  ordersummary:any = {
    combocode: []
  };
  id:any;
  orderid :any;
  form:FormGroup;
  shippingform:FormGroup;
  invoice_file:any;
  scanfile:any;
  retailerid:any;
  constructor(
    private api: OrdersService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public customer:CustomerService,
    private excelService:ExcelService
  ) {

    this.form = fb.group ({
      'comment':['']
    });



    this.shippingform = fb.group ({
      'invoice_id':[''],
      'invoice_qty':[''],
      'invoice_value':[''],
      'invoice_file':[''],
      'invoice_file_name':[{value:'',disabled:true}],
      'transportName':[''],
      'trackingID':[''],
      'Shipmentdate':[''],
      'DeliveryDate':['']
    })

  }

  ngOnInit() {

    this.id = this.route.snapshot.params['brandid'];;
    let orderid = this.route.snapshot.params['orderid'];
    this.retailerid = this.route.snapshot.params['retailerid'];
    this.orderid = orderid;
    this.getorderData();
    this.getordersummary(orderid);

  }
  uploadpp(event){
      if(event.target.files.length > 0) {
        let file = event.target.files[0];
        console.log(file);
        this.shippingform.get('invoice_file_name').setValue(file.name);
        this.invoice_file = file;

      }
  }



  getorderData(){
    this.api.getOrderDetail(this.orderid,this.id,this.retailerid)
      .subscribe(
        response => {
          console.log(response);
           this.orderDetailsData = response.data;
           this.setorderstatus(response.data);
        },
        response => {
          alert(response.error.error);
        });
  }

  setorderstatus(response) {
    if(response != ''){
      this.orderstatus = response[0].data[0].data[0];
      console.log(this.orderstatus);
    }
  }

  approveOrder() {
    console.log('Order Approved');

    this.approvedSelected = true;

    // this.api.approveOrder(this.orderDetailsData._id)
    //   .subscribe(
    //     response => {
    //       console.log(response);
    //       if(response.status == "success") {
    //         this.router.navigateByUrl('apps/b2b-administration/orders');
    //       }
    //     },
    //     response => {
    //       alert(response.error.error);
    //     });
  }
  cancelOrderDate() {
    this.approvedSelected = false;

  }

  ShipAPICALL(){
  const data = this.shippingform.value;
  console.log(this.ordersummary);
    this.api.shipnow(this.orderid,data,this.ordersummary,this.invoice_file)
      .subscribe(
        response => {
            this.router.navigateByUrl('apps/b2b-administration/orders');
        },
        response => {
          alert(response.error.error);
        });
  }




  editOrder() {
    console.log('Edit Approved');
    this.readonlyInput = false;
    this.onEdit = true;

  }


  rejectOrder() {
    this.api.rejectOrder(this.orderid,this.form.get('comment').value)
      .subscribe(
        response => {
          console.log(response);
          if(response.status) {
            this.router.navigateByUrl('apps/b2b-administration/orders');
          }
        },
        response => {
          alert(response.error.error);
        });
  }

  cancelOrder() {
    console.log('Cancel edit order');
    this.readonlyInput = true;
    this.onEdit = false;
  }

  resendOrder(ordersummary) {


    this.api.brandresendorder(this.orderid,ordersummary)
      .subscribe(
        response => {
            this.router.navigateByUrl('apps/b2b-administration/orders');
        },
        response => {
          alert(response.error.error);
        });
  }


  purchaseOrder(){
    window.open(this.customer.AdminBaseurl + 'pdf/generatePoForRetailer/'+this.orderid);
  }

  getordersummary(orderid){
    this.api.getordersummary(orderid).subscribe(response => {
      this.ordersummary = response.data[0];
      this.dispatchTerms = response.dispatchTermData;
    });


  }

  shippedvalues(){
    this.shippingform.patchValue({
      'invoice_qty': this.ordersummary.totalquantity,
      'invoice_value': (this.ordersummary.totalAmount + this.ordersummary.tax).toFixed(2)
    });
  }

  showproduct(data,ean,styleindex,colorindex,sizeindex){
    console.log(data);
    var productid = 'P' + styleindex + colorindex + sizeindex ;
    var value = parseFloat((document.getElementById(productid) as HTMLInputElement).value);
if(data.orderStatus == 'approved' ){
  if(value <= data.cartdata.retailer_revised_qty){
            this.api.updateshipquantity(ean,this.orderid,value).subscribe(response=>{
            this.setvaliddata(productid,data,value);
            this.getordersummary(this.orderid);
            this.getorderData();
          });
        }
        else {
          this.setinvaliddata(productid);
        }
}
else
{      if(value <= data.cartdata.input_qty){

        this.api.updatequantity(ean,this.orderid,value).subscribe(response=>{
          this.setvaliddata(productid,data,value);
          this.getordersummary(this.orderid);
          this.getorderData();
        });
      }
      else {
        this.setinvaliddata(productid);
      }
}
  }

  setinvaliddata(productid){
    var divToChange = document.getElementById(productid);
    divToChange.className = "number-input error";
  }

  setvaliddata(productid,data,value){
    console.log(data);
    if(data.cartdata.revised_qty != data.cartdata.input_qty){
      console.log("Revised Quantity");
      var divToChange = document.getElementById(productid);
      divToChange.className = "number-input editeddata";
    }
    else if(value == data.cartdata.input_qty){
    var divToChange = document.getElementById(productid);
    divToChange.className = "number-input";}
  }

  sizedata(data){
    var order = ["FS","XXXS","XXS",'XS','S','M','L','XL','XXL',"XXXL","XXXXL","XSS","SM","ML","LXL","NB","00","02","04","06","08","10","12","14","16","18","20","22","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","42","44","46","48","50","52","54","56","58"];;
    var array = [];
    for(let x of order){
      for (let y of data){
        if(x == y.cartdata.catalogue.Size_Code){
            array.push(y);
        }
      }
    }
    return array;
  }

  openinvoice(url){
    window.open(url,'_blank');
  }

  downloadExecelApi(){
    this.api.getorderdataexcel(this.orderid).subscribe(response => {
      this.donwloadOrderExcel(response)
    })
  }
  donwloadOrderExcel(response){
    if(response.status){
      this.excelService.exportAsExcelFile(response.data, 'order');
    }
  }


}
