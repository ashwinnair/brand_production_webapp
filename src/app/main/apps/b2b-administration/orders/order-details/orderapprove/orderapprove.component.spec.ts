import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderapproveComponent } from './orderapprove.component';

describe('OrderapproveComponent', () => {
  let component: OrderapproveComponent;
  let fixture: ComponentFixture<OrderapproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderapproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderapproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
