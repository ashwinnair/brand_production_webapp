import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipnowComponent } from './shipnow.component';

describe('ShipnowComponent', () => {
  let component: ShipnowComponent;
  let fixture: ComponentFixture<ShipnowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipnowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipnowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
