import { Component, OnInit,Inject } from '@angular/core';
import { OrdersService } from '../../orders.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';
@Component({
  selector: 'app-shipnow',
  templateUrl: './shipnow.component.html',
  styleUrls: ['./shipnow.component.scss']
})
export class ShipnowComponent implements OnInit {


  constructor(private api: OrdersService,
  private router: Router,
  private route: ActivatedRoute,
  private fb: FormBuilder,
  public ngxSmartModalService: NgxSmartModalService) {

   }

  ngOnInit() {
  }


}
