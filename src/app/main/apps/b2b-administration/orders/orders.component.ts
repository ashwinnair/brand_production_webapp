import {Component, OnInit, ViewChild} from '@angular/core';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  activeTabs: string = 'pending';
  activeTabType: string = 'ror';
  handleChangeValue: string = 'pending';

  constructor() { }


  ngOnInit() {

  }

  selectedItem(data) {
    console.log(data);
    this.activeTabs = data;
  }

  selectedType(data) {
    this.activeTabType = data;
  }
  
  handleChange(data) {
    // console.log(data);
    this.handleChangeValue = data;
  }




}
