import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { CustomerService } from '../../customer.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  orderID : any;

  constructor(
    private http: HttpClient,
    private customer: CustomerService
  ) {
  }

  getOrderID(data) {
    this.orderID = data;
    console.log(this.orderID);
  }

  getOrderDetail(orderid,brandid,retailerid): Observable<any>{

    return this.http.get<any>(this.customer.AdminBaseurl + 'neworder/orderlisting/' + orderid);

  }

  getDistributorOrderDetail(orderid,brandid,distributorid): Observable<any>{

    return this.http.get<any>(this.customer.AdminBaseurl + 'distributorOrder/orderListing/' + orderid);

  }

  approveOrder(data): Observable<any>{

    return this.http.put<any>(this.customer.AdminBaseurl + 'neworder/approveOrder',data );

  }

  approveDistributorOrder(data): Observable<any>{

    return this.http.put<any>(this.customer.AdminBaseurl + 'distributorOrder/approveDistributorOrder',data );

  }

  rejectOrder(data,comment): Observable<any>{

    return this.http.put<any>(this.customer.AdminBaseurl + 'neworder/rejectOrder', {
        orderId: data,
        rejectedBy: "brand",
        comment: comment
    } );

  }

  rejectDistributorOrder(data,comment): Observable<any>{

    return this.http.put<any>(this.customer.AdminBaseurl + 'distributorOrder/rejectOrder', {
        orderId: data,
        rejectedBy: "brand",
        comment: comment
    } );

  }

  brandresendorder(data,ordersummary): Observable<any>{

    return this.http.put<any>(this.customer.AdminBaseurl + 'neworder/brandresendorder',{
      orderId : data,
      revised_po_qty : ordersummary.totalquantity,
      revised_po_value : (ordersummary.totalAmount + ordersummary.tax)
    } );

  }

  brandresenddistributororder(data,ordersummary): Observable<any>{

    return this.http.put<any>(this.customer.AdminBaseurl + 'distributorOrder/brandResendDistributorOrder',{
      orderId : data,
      revised_po_qty : ordersummary.totalquantity,
      revised_po_value : (ordersummary.totalAmount + ordersummary.tax)
    } );

  }


  shipnow(id,data,ordersummary,invoice_file): Observable<any>{

    const body = new FormData();

    body.append('shipmentDispatchDate', data.Shipmentdate);
    body.append('deliveryReceivedDate', data.DeliveryDate);
    body.append('deliveryReceivedQty', ordersummary.totalquantity);
    body.append('deliveryReceivedValue', ordersummary.totalAmount + ordersummary.tax);
    body.append('transportName', data.transportName);
    body.append('trackingId', data.trackingID);
    body.append('invoiceId', data.invoice_id);
    body.append('invoiceFile', invoice_file);
    body.append('order', id);
    body.append('invoiceQty',data.invoice_qty);
    body.append('invoiceValue',  data.invoice_value);

    return this.http.post<any>(this.customer.AdminBaseurl + 'shipment/orderShipment',body);
  }

  distributorshipnow(id,data,ordersummary,invoice_file): Observable<any>{

    const body = new FormData();

    body.append('shipmentDispatchDate', data.Shipmentdate);
    body.append('deliveryReceivedDate', data.DeliveryDate);
    body.append('deliveryReceivedQty', ordersummary.totalquantity);
    body.append('deliveryReceivedValue', ordersummary.totalAmount + ordersummary.tax);
    body.append('transportName', data.transportName);
    body.append('trackingId', data.trackingID);
    body.append('invoiceId', data.invoice_id);
    body.append('invoiceFile', invoice_file);
    body.append('order', id);
    body.append('invoiceQty',data.invoice_qty);
    body.append('invoiceValue',  data.invoice_value);

    return this.http.post<any>(this.customer.AdminBaseurl + 'distributorShipment/distributorOrderShipment',body);
  }

  purchaseorderfun(data): Observable<any>{

    return this.http.get<any>(this.customer.Baseurl + 'order/generatepurchaseorder/5c99f76f9fe0d7247473b674');

  }

  getordersummary(orderid) : Observable<any>{
        return this.http.get<any>(this.customer.AdminBaseurl + 'neworder/ordersummary/'+ orderid);
  }

  getdistributorordersummary(orderid) : Observable<any>{
        return this.http.get<any>(this.customer.AdminBaseurl + 'distributorOrder/orderSummary/'+ orderid);
  }

  getquantity(ean): Observable<any>{

    return this.http.get(this.customer.Baseurl + 'inventory/getquantity/'+ ean);
  }

  updatequantity(ean,orderid,value):Observable<any>{

    const body ={
    "orderId" : orderid ,
    "EAN_no": ean,
    "revised_qty": value
    }

    return this.http.put(this.customer.AdminBaseurl + 'neworder/editOrder',body);
  }

  distributorupdatequantity(ean,orderid,value):Observable<any>{

    const body ={
    "orderId" : orderid ,
    "EAN_no": ean,
    "revised_qty": value
    }

    return this.http.put(this.customer.AdminBaseurl + 'distributorOrder/editDistributorOrder',body);
  }

  updateshipquantity(ean,orderid,value):Observable<any>{

    const body ={
    "orderId" : orderid ,
    "EAN_no": ean,
    "shipped_qty": value
    }

    return this.http.put(this.customer.AdminBaseurl + 'neworder/shipmenteditOrder',body);
  }

  distributorupdateshipquantity(ean,orderid,value):Observable<any>{

    const body ={
    "orderId" : orderid ,
    "EAN_no": ean,
    "shipped_qty": value
    }

    return this.http.put(this.customer.AdminBaseurl + 'distributorOrder/shipmentEditDistributorOrder',body);
  }

  getpurchaseorderdetails(orderid):Observable<any>{
    return this.http.get(this.customer.AdminBaseurl + 'neworder/purchaseDetails/' + orderid);
  }

  getdistributorpurchaseorderdetails(orderid):Observable<any>{
    return this.http.get(this.customer.AdminBaseurl + 'distributorOrder/purchaseDetails/' + orderid);
  }

  getorderdataexcel(orderid) : Observable<any>{

     return this.http.get<any>(this.customer.AdminBaseurl + 'neworder/orderDetailsForExcelDownload/'+ orderid );
  }

}
