import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsorApprovedComponent } from './rsor-approved.component';

describe('RsorApprovedComponent', () => {
  let component: RsorApprovedComponent;
  let fixture: ComponentFixture<RsorApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsorApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsorApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
