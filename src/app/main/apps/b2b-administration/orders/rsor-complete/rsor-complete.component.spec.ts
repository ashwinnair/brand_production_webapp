import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsorCompleteComponent } from './rsor-complete.component';

describe('RsorCompleteComponent', () => {
  let component: RsorCompleteComponent;
  let fixture: ComponentFixture<RsorCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsorCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsorCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
