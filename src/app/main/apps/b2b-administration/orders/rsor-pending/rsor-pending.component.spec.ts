import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsorPendingComponent } from './rsor-pending.component';

describe('RsorPendingComponent', () => {
  let component: RsorPendingComponent;
  let fixture: ComponentFixture<RsorPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsorPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsorPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
