import {Component, ViewChild, OnInit} from '@angular/core';
import { OrdersService } from '../orders.service';
import { Router } from '@angular/router';
import { CustomerService } from '../../../customer.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ExcelService } from '../../../../../services/excel.service';


@Component({
  selector: 'app-rsor-pending',
  templateUrl: './rsor-pending.component.html',
  styleUrls: ['./rsor-pending.component.scss']
})
export class RsorPendingComponent implements OnInit {

  getAllPendingOrder: any;
  id:any;
  datas:any;
  dataArrayEmpty = true;


  constructor(
    private api: OrdersService,
    private router: Router,
    private customer: CustomerService,
    private http:HttpClient,
    private excelService:ExcelService
  ) {
    const id = JSON.parse(localStorage.getItem('userBrand'));
    this.id = id.userData.companyId;

  }

  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.datas, 'linsheet');
  }

  ngOnInit() {

    this.http.get(this.customer.AdminBaseurl + 'neworder/brandOrderListing/' + this.id + '/pending/SOR').subscribe(
      response => {
        this.setdata(response);
      }
    )
  }

  setdata(response){
    this.datas = response.data;

    if(this.datas.length > 0){
      this.dataArrayEmpty = false;
    }
  }
  
  someClickHandler(data) {
    console.log(data);
    this.api.getOrderID(data._id)
    this.router.navigateByUrl('/apps/b2b-administration/orders/details/' + data._id + '/' + data.brand_id._id + '/' + data.retailer_id._id);
  }

}
