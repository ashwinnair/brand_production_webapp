import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsorRevisedComponent } from './rsor-revised.component';

describe('RsorRevisedComponent', () => {
  let component: RsorRevisedComponent;
  let fixture: ComponentFixture<RsorRevisedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsorRevisedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsorRevisedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
