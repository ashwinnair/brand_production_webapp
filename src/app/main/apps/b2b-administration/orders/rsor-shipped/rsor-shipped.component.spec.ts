import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsorShippedComponent } from './rsor-shipped.component';

describe('RsorShippedComponent', () => {
  let component: RsorShippedComponent;
  let fixture: ComponentFixture<RsorShippedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsorShippedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsorShippedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
