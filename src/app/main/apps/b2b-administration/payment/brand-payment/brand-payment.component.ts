import {Component, OnInit, ViewChild} from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { OrdersService } from '../../orders/orders.service';
import { Router } from '@angular/router';
import { CustomerService } from '../../../customer.service';


@Component({
  selector: 'app-brand-payment',
  templateUrl: './brand-payment.component.html',
  styleUrls: ['./brand-payment.component.scss']
})
export class BrandPaymentComponent implements OnInit {

    // Must be declared as "any", not as "DataTables.Settings"
    dtOptions: any = {};
    persons: any[];
    id:any;
    @ViewChild('dataTable') table;
    dataTable:any;
    dtOption: any = {};

    constructor(
      private api: OrdersService,
      private router: Router,
      private customer: CustomerService
    ) {

      const id = JSON.parse(localStorage.getItem('userBrand'));
      this.id = id.userData.companyId;
    }


    ngOnInit() {

      this.dtOptions = {
        "ajax": {
          url: this.customer.AdminBaseurl + 'neworder/brandOrderListing/' + this.id + '/complete/OR',
          type: 'GET'
        },
        columns: [
              {
                title: 'Brand',
                data: 'brand_id.brand_name'
              },
              {
                title: 'Retailer',
                data: 'retailer_id.retailer_name'
              },
              {
                  title: 'PO #',
                  data: 'purchase_order_number'
              },
              {
                  title: 'Invoice #',
                  data: 'shipment.invoiceId'
              },
              {
                  title: 'Invoice Date',
                  render: function (data: any, type: any, full: any) {
                    var date = new Date(full.approvedDate);
                      return date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear();
                  }
              },
              {
                  title: 'Invoice Amount',
                  render: function (data: any, type: any, full: any) {
                    var value = Math.round(parseFloat(full.shipment.invoiceValue)*100)/100;
                    return value.toLocaleString();
                  }
              },
              {
                  title: 'Payment Due Date',
                  render: function (data: any, type: any, full: any) {
                    var date = new Date(full.approvedDate);
                      return date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear();
                  }
              },
              {
                  title: 'Payment Date',
                  render: function (data: any, type: any, full: any) {
                    var date = new Date(full.expectedShipmentDate);
                    return date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear();
                  }
              },
              {
                  title: 'Payment Amount',
                  render: function (data: any, type: any, full: any) {
                    var value = Math.round(parseFloat(full.shipment.invoiceValue)*100)/100;
                    return value.toLocaleString();
                  }
              },
            {
                title: 'RTV Comments',
                render: function (_data: any, type: any, full: any) {
                  if(full.shipment.rtvComments != null){
                    return full.shipment.rtvComments
                  }
                  else{
                  return 'No Comments';}
                }
            },
            {
                title: 'Bank Transfer ID',
                render: function (data: any, type: any, full: any) {
                  return 'HDFC10021';
                }
            },
            {
              title: 'Action',
              render: function (data: any, type: any, full: any) {
                return '<a class="view_details">View Details</a>';
              }
            }
          ],
          rowCallback: (row: Node, data: any[] | Object, index: number) => {
            const self = this;
            $('.view_details', row).unbind('click');
            $('.view_details', row).bind('click', () => {
              self.someClickHandler(data);
            });
            return row;
          },
        dom: 'Rt,Bfrtip',
        // Use this attribute to enable colreorder
        // colReorder: {
        //   order: [0,1],
        // },
        button:[
           'colvis',
           'copy',
           'print',
           'excel'
        ]
      };
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.DataTable(this.dtOptions);
    }


    someClickHandler(data) {
      console.log(data);
      this.api.getOrderID(data._id)
      this.router.navigateByUrl('/apps/b2b-administration/orders/details/' + data._id + '/' + data.brand_id._id + '/' + data.retailer_id._id);
    }

}
