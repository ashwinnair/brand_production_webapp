import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributorPaymentsComponent } from './distributor-payments.component';

describe('DistributorPaymentsComponent', () => {
  let component: DistributorPaymentsComponent;
  let fixture: ComponentFixture<DistributorPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributorPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributorPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
