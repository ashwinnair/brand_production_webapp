import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerPaymentsComponent } from './retailer-payments.component';

describe('RetailerPaymentsComponent', () => {
  let component: RetailerPaymentsComponent;
  let fixture: ComponentFixture<RetailerPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
