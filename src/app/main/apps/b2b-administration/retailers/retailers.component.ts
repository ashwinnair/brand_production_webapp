import {Component, OnInit, ViewChild} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerService } from '../../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-retailers',
  templateUrl: './retailers.component.html',
  styleUrls: ['./retailers.component.scss']
})
export class RetailersComponent implements OnInit {

  allRetailers: any;
  companyId:any;


  constructor(
    private http: HttpClient,
    private customer: CustomerService,
    private router:Router
  ) {
  }

  ngOnInit() {

    const id = JSON.parse(localStorage.getItem('userBrand'));
    this.companyId = id.userData.companyId;

    this.http.get(this.customer.AdminBaseurl + 'connection/allDetailsByCompanyId/'+ this.companyId)
      .subscribe(
        response => {

          this.setretailer(response)

        },
        response => {
          alert(response);
        });

  }

  setretailer(response){
    if(response.data.length > 0)
    this.allRetailers = response.data;
    console.log(this.allRetailers);
  }

  gotoretailer(data){
    localStorage.setItem('retailerprofile',JSON.stringify(data));
    this.router.navigateByUrl('/apps/profile-retailer/' + data._id)
  }
}
