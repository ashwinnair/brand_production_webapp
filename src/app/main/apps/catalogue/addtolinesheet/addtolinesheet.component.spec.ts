import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtolinesheetComponent } from './addtolinesheet.component';

describe('AddtolinesheetComponent', () => {
  let component: AddtolinesheetComponent;
  let fixture: ComponentFixture<AddtolinesheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtolinesheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtolinesheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
