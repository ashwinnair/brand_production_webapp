import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CustomerService } from '../../customer.service';
import { CatalogueService } from '../catalogue.service';
import { ExcelService } from '../../../../services/excel.service';

@Component({
  selector: 'app-addtolinesheet',
  templateUrl: './addtolinesheet.component.html',
  styleUrls: ['./addtolinesheet.component.scss']
})
export class AddtolinesheetComponent implements OnInit {

  id:any;
  linesheetdetails:any = {
    data: []
  };
  products:any =[];
  columns:any = [];
  columnvis:boolean = false;
  activatefilter:boolean = true;
  filterbody:any = {} ;
  keys = [];
  FilterData:any;
  filter = false;
  filterform:FormGroup;
  checkbox:any;
  filtercheckbox:any= [];
  filtertag:any = [];
  filterkeys:any = [];
  orderedfilter:any =[];
  selectedproducts = [];
  checkboxes:any = [];
  selectedcheckboxes:any = [];
  allchecked:boolean  = false;
  filtergroup:any = [];
  searchfetchcount:number = 0;
  columnviscount:number =0;
  fitlercount:number = 0;
  preview:any = false;
  allselectedchecked:any =false;
  linesheetResult = [];
  deletedproducts:any = [];

  constructor( private router: Router,
      private route: ActivatedRoute,
      private customer: CustomerService,
      private fb: FormBuilder,
      public http: HttpClient,
      public api:CatalogueService,
      private excelService:ExcelService
     ) { }

  ngOnInit() {
        this.id = this.route.snapshot.params['linesheetid'];
        this.getlinesheetdetails();
        this.setcolumns();

  }

  exportAsXLSX():void {
    console.log(this.selectedproducts);
    this.excelService.exportAsExcelFile(this.selectedproducts, 'linsheet');
}

  getlinesheetdetails(){
    this.http.get(this.customer.AdminBaseurl + 'lineSheet/detailsOfLinesheetByCode/'+ this.id).subscribe(response => {
      this.linesheetdetails = response;
      // this.getcatalogue(this.linesheetdetails);
      this.getFilters({});
    })

  }

  getcatalogue(id){
    var brandid = id.data[0].brandId._id
    this.http.get(this.customer.Baseurl + 'catalogue/newcataloguelistbybrand/'+ brandid)
    .subscribe(response => {
      this.products =response;
    })
  }


  activatepreview(){
    if(this.preview)
    {
      this.preview = false
    }
    else
    {
      this.preview = true;
      this.activatefilter = false;
      this.columnvis = false;
    }
    this.getPreview();
    this.linesheetResult = [];
    this.allchecked = false;

  }

  getPreview(){
    this.http.get(this.customer.AdminBaseurl + 'lineSheetCatalogue/cataloguesByLinesheet/'+this.linesheetdetails.data[0]._id).
    subscribe(response=>{
      this.setselectedproductsapi(response);
    })
  }

  setselectedproductsapi(response){
    this.selectedproducts = [];
    if(response.data.length > 0){
      for(let x of response.data){
        this.selectedproducts.push(x.catalogueId);
      }
    }
    console.log(this.selectedproducts);
  }

  setcolumns(){
  this.columns = [
{ value: 'EAN Code',  visibility : true},
{ value:'Style code', visibility : true },
{ value:'Style Name', visibility : true },
{ value:'Style Description', visibility : true },
{ value:'IK Colour Name', visibility : true },
{ value:'Size Code', visibility : true },
{ value:'MRP', visibility : true },
{ value:'Season name', visibility : false },
{ value:'Season Code', visibility : false },
{ value:'Year', visibility : false },
{ value:'Segment', visibility : true },
{ value:'Portfolio', visibility : true },
{ value:'Department', visibility : true },
{ value:'Vertical', visibility : true },
{ value:'Category', visibility : true },
{ value:'Brick', visibility : true },
{ value:'Occasion', visibility : false },
{ value:'Sleeve', visibility : false },
{ value:'Neck', visibility : false },
{ value:'Collar', visibility : false },
{ value:'Pattern', visibility : false },
{ value:'Fabric', visibility : false },
{ value:'Fit', visibility : false },
{ value:'Material', visibility : false },
{ value:'Outer Material', visibility : false },
{ value:'Inner Material', visibility : false },
{ value:'Sole Material', visibility : false },
{ value:'Insole', visibility : false },
{ value:'leather type', visibility : false },
{ value:'Closure type', visibility : false },
{ value:'Strap type', visibility : false },
{ value:'Strap drop length unit', visibility : false },
{ value:'Strap drop length', visibility : false },
{ value:'Number of compartments', visibility : false },
{ value:'Composition', visibility : false },
{ value:'Weave', visibility : false },
{ value:'Technique', visibility : false },
{ value:'Length', visibility : false },
{ value:'Hemline', visibility : false },
{ value:'Rise', visibility : false },
{ value:'Pack Qty', visibility : false },
{ value:'Pleat', visibility : false },
{ value:'Closure type', visibility : false },
{ value:'Weather', visibility : false },
{ value:'Height map', visibility : false },
{ value:'Width', visibility : false },
{ value:'toe style', visibility : false },
{ value:'Arch', visibility : false },
{ value:'Cleats', visibility : false },
{ value:'Cushioning', visibility : false },
{ value:'heel type', visibility : false },
{ value:'Heel height unit', visibility : false },
{ value:'Heel height', visibility : false },
{ value:'platform height unit', visibility : false },
{ value:'platform height', visibility : false },
{ value:'Technology', visibility : false },
{ value:'Dimensions unit', visibility : false },
{ value:'Height (H)', visibility : false },
{ value:'Length (L)', visibility : false },
{ value:'Width (W)', visibility : false },
{ value:'Style type', visibility : false },
{ value:'Special Feature', visibility : false },
{ value:'Special Feature 1', visibility : false },
{ value:'Special Feature 2', visibility : false },
{ value:'Special Feature 3', visibility : false }
   ]
   this.setcolviscount();
  }

togglecolumnvis(){
  if(this.columnvis){
  this.columnvis = false;}
  else
  this.columnvis = true
}
setcolviscount(){
  var count = this.columns.filter((obj) => obj.visibility === true).length;
  this.columnviscount = count;
}

togglefilter(){
  if(this.activatefilter){
  this.activatefilter = false;
  }
  else
  {
    this.activatefilter = true
    this.columnvis = false;
    this.preview = false;
  }
  }

  filters:any =[];

  getFilters(filter){
    const body = filter;
    this.filters=[];
    this.api.getFilters(this.linesheetdetails.data[0].brandId._id,body).subscribe(response => {
      this.FilterData = response.data;

          var order =
          ["portfolio","segment","department","vertical","category","brick","occasion","sleeve","neck","collar",
          "pattern","fabric","fit","material","composition","weave","technique","length","hemline","rise","Size_Code","Color_code"];

      let filterkeys = (Object.keys(this.FilterData));
      let index = 0;
      for(let count = 0; count< filterkeys.length; count++){
      if(this.FilterData[filterkeys[count]].length >0)
       {
           this.filters.push({
           index: index,
           filtername: filterkeys[count],
           filters: this.FilterData[filterkeys[count]]
         });
         index = index+1;
      }
      }
      var orderedarray =[];
      for(let x of order){
        for(let y of this.filters){
          if(x == y.filtername){
            orderedarray.push(y);
          }
        }
      }
        this.getPreview();
    this.orderedfilter = orderedarray;
    });
}



formthebody(event,filtergroup){
  console.log("Event: ", event);

  var key = filtergroup.filtername;
  var value= [];
  // var value = event.source.triggerValue.split(',');

  for(let x of event.value){
    value.push(x._id);
  }
  if(value.length == 0){
    delete this.filterbody[key];
  }
  else{
  if(this.filterbody[key] == undefined){
    this.filterbody[key] = value;
  }
  else if(this.filterbody[key].length > 0){
    if(this.filterbody[key].indexOf(value) == -1){
    this.filterbody[key]=value;
    }
    else if(this.filterbody[key].indexOf(value) > 0){
      this.filterbody[key].splice(this.filterbody[key].indexOf(value),1,value);
    }
  }
}
  console.log("Filter API Body : ",this.filterbody);
  this.fitlercount = this.bodycount(this.filterbody);
}

bodycount(obj) {
  var count=0;
    for(var prop in obj) {
      console.log(obj.hasOwnProperty(prop));
       if (obj.hasOwnProperty(prop)) {
          console.log(this.filterbody[prop].length);
            count = count + this.filterbody[prop].length;
       }
    }
    return count;
}

getProducts(){
  this.preview = false;
    this.searchfetchcount++;
    this.checkboxes = [];
    this.api.getProducts(this.linesheetdetails.data[0].brandId._id,this.filterbody).subscribe(response => {
      this.linesheetResult = response.data;
      for(let i of this.linesheetResult){
        for(let product of i.data){
          this.checkboxes.push(false);
          console.log("ARRAY:",this.checkboxes);
        }
      }
    })

}

selectProduct(event,product,checkboxindex){

  if(event.checked == true){
    var index = this.selectedproducts.findIndex(products => products.EAN_code === product.EAN_code)
    console.log(index);
    this.selectedproducts.push(product);
    this.checkboxes[checkboxindex] = true;
        this.addtolinesheet();
  }
  else if(event.checked == false){
    var index = this.selectedproducts.findIndex(products => products.EAN_code === product.EAN_code);
    this.selectedproducts.splice(index,1);
    this.checkboxes[index] = false;
    this.deletedproducts.push(product);

    this.deletelinesheet();
  }

  console.log(this.deletedproducts);
  console.log(this.checkboxes.indexOf(false));
  if(this.checkboxes.indexOf(false) > -1){
    this.allchecked = false;
  }
  else if(this.checkboxes.indexOf(false) == -1)
  {
    this.allchecked = true;
  }



}

selectAllProducts(event){
  this.allchecked = true;
  if(event.checked == true){
    for(let i =0; i<this.linesheetResult.length;i++){
      for(let v = 0; v < this.linesheetResult[i].data.length; v++){
        this.checkboxes.splice(v,1,true);
        var index = this.selectedproducts.findIndex(products => products.EAN_code === this.linesheetResult[i].data[v].EAN_code);
        if(index == -1){
          this.selectedproducts.push(this.linesheetResult[i].data[v]);
        }
      }
    }
        this.addtolinesheet();
  }
  else if(event.checked == false){
    this.checkboxes= []
    for(let i =0; i<this.linesheetResult.length;i++){
      for(let v = 0; v < this.linesheetResult[i].data.length; v++){
        this.checkboxes.push(false);
        var index = this.selectedproducts.findIndex(products => products.EAN_code === this.linesheetResult[i].data[v].EAN_code);
        this.selectedproducts.splice(index,1);
        this.deletedproducts.push(this.linesheetResult[i].data[v])
      }
    }
    console.log(this.deletedproducts);
    this.deletelinesheet();
  }

}

selectAllselectedProducts(event){
  this.allselectedchecked = true;
  this.setpreviewcheckbox();
}

setpreviewcheckbox(){
  for(let x of this.selectedproducts){
    this.selectedcheckboxes.push(false);
  }
}

deleteallproduct(){
  this.deletelinesheet();
  const body = new FormData();
  body.append('lineSheetId',this.linesheetdetails.data[0]._id);
  for(let i=0;i<this.selectedproducts.length;i++){
    var key = 'catalogueId'+i;
    body.append(key,this.selectedproducts[i]._id);
  }
  this.api.deletelinesheet(body).subscribe(
    response => {
    this.getPreview();
    this.selectedproducts = [];
    }
  )
}

deleteproduct(i){

  const body = new FormData();
  body.append('lineSheetId',this.linesheetdetails.data[0]._id);
  body.append('catalogueId0',this.selectedproducts[i]._id);
  this.api.deletelinesheet(body).subscribe(
    response => {
      this.getPreview();
    this.selectedproducts.splice(i,1);
    }
  )

}

addtolinesheet(){
  const body = new FormData();
  body.append('lineSheetId',this.linesheetdetails.data[0]._id);
  var productcodes = []
  for(var i= 0; i < this.selectedproducts.length; i++){
    var key = 'catalogueId' + i;
  body.append(key,this.selectedproducts[i]._id);
  }

  this.api.sendlinesheet(body).subscribe(
    response => {
      console.log(response);
    }
  )
}
deletelinesheet(){
  console.log("Delete Products",this.deletedproducts);
  const body = new FormData();
  body.append('lineSheetId',this.linesheetdetails.data[0]._id);
  var productcodes = []
  for(var i= 0; i < this.deletedproducts.length; i++){
    var key = 'catalogueId' + i;
  body.append(key,this.deletedproducts[i]._id);
  }

  this.api.deletelinesheet(body).subscribe(
    response => {
      this.deletedproducts = [];
      this.selectedproducts = [];

    }
  )
}

activatelinesheet(brand){
  var value;
  if(brand){
    value = false;
  }
  else{
    value = true;
  }
  const body = new FormData();
  body.append('publish',value);
  this.http.patch(this.customer.AdminBaseurl+ 'lineSheet/updateLinesheet/'+this.linesheetdetails.data[0]._id,body)
  .subscribe(
    response => {
      console.log(response);
    }
  )
}


}
