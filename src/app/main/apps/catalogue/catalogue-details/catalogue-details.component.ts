import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { CustomerService } from '../../customer.service';
import { EventEmiterService } from 'app/event.emmiter.service';
import { CatalogueService } from '../catalogue.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ImageZoomModule } from 'angular2-image-zoom';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import { $ } from 'protractor';
import { from } from 'rxjs/observable/from';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-catalogue-details',
  templateUrl: './catalogue-details.component.html',
  styleUrls: ['./catalogue-details.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})




export class CatalogueDetailsComponent implements OnInit {

  imageMode:any = true;
  defaultImage = 'https://www.turfgrasssod.org/wp-content/themes/linstar/assets/images/default.jpg';
  data:any;
  dataindex:any[]=[];
  imageSrc: any;
  selectedImage : Number;
  catalogue: any;
  productImages: any;
  size:any[]= [];
  modalstatus:number =1;
  catalogueID: any;
  subscription: Subscription;
  Grades:any[]=[] ;
  colors:any[]=[];
  BrandData:any;
  BrandID:any;
  ConnectionID :any;
  CartID :any;
  retailerID :any;
  colorcodes:any[] =[];
  catalogueimages:any[]=[];
  colorname:any[]=[];
  colors1:any;
  showGrades:any[]=[];
  showGradesPopup:any[]=[];
  value = 0;
  catalogueColorCodes: any;
  colorindex :number =0;
  mainimage:any;
  stylecode:any;
  colorGradesGroup: any[] = [];
  colorGrade:any=[];
  colorGradesValue: string;
  counter:any;
  datas:any = [];
  colorGrades: any[] = [
    {id:"Grade A",value:'A'},
    {id:"Grade B",value:'B'},
    {id:"Grade C",value:'C'},
    {id:"Grade D",value:'D'},
  ];
  order =  ["XS","S","M","L","XL","XXL"];
  zoomedImageSrc:any;
  height:any =500;
  width:any = 500;

  constructor(
    private http: HttpClient,
    private customer: CustomerService,
    private EventEmiter: EventEmiterService,
    private api: CatalogueService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.catalogueColorCodes = JSON.parse(localStorage.getItem('catalogueData'));
    this.stylecode = this.route.snapshot.paramMap.get('stylecode');
    this.counter = localStorage.getItem('count');

    let vendorCode = this.route.snapshot.paramMap.get('vendorcode');
    this.http.get(this.customer.AdminBaseurl + 'brand/brandDetailsByVendorCode/' + vendorCode )
      .subscribe(
        response => {
          this.setBrandData(response);
        });


  }
  setBrandData(response){
    if(response.status){
      this.BrandID = response.data._id;

      this.getproductinfo();

      this.subscription = this.EventEmiter.getCatalogueID().subscribe(data => {
        this.catalogueID = data;
      });

      for(let i of this.catalogueColorCodes.colorCodes){
        this.showGrades.push(false);
        this.showGradesPopup.push(false);
        this.Grades.push(1);
      }
    }

  }

  ngOnInit() {

    }

    setcolor(response){
      if(response != ''){
        let colorcodes = this.catalogue.Color_code;
        // this.colors1 = colorcodes.split(',');
        this.colors1 = colorcodes;
      }
      for(let i of this.colors1){
        this.showGrades.push(false);
        this.showGradesPopup.push(false);
        this.Grades.push(1);
      }
    }

    getColorGrade(i, grade) {
      console.log("Grade", i, grade);
      if(this.showGradesPopup[i] == true){
          this.colorGrade.splice (i,1,{
          color : this.colors[i],
          colorGrade: grade
        })
      }

      if(this.showGrades[i] == false && this.showGradesPopup[i] == false){
      this.showGrades.splice(i,1,true);}
      else if (this.showGrades[i] == true){
        this.showGrades.splice(i,1,false);
      }

        console.log(this.colorGrade);
    }



    selectColor(data, i) {
      if(this.showGrades[i] == false && this.showGradesPopup[i] == false){
      this.showGrades.splice(i,1,true);}
      else if (this.showGrades[i] == true){
        this.showGrades.splice(i,1,false);
      }
    }

    getColorData(index) {

      this.colorindex = index;
      this.mainimage = this.datas[index][0].catalogue_imgs[0];
      this.productImages = this.catalogueimages[index];

    }

    getproductinfo(){

      this.http.get(this.customer.AdminBaseurl + 'catalogue/catalogeDetailByStyleCode/'+ this.BrandID + '/' + this.stylecode)
      .subscribe(response=>{
        this.setcolorstyle(response);
      });
    }

    setcolorstyle(response){
      if(response != ''){
        let value = response.data;
        this.data = value[0];
        this.pipedata(response.data);
      }
    }

    pipedata(data){
      const source = from(data);
      const output = <any>source.pipe(
        groupBy(value  => (value as any).Color_code[0]),
        mergeMap(group => group.pipe(toArray()))
      );
      const subscribe = output.subscribe(val => {
        var array =[];
        for(let x of this.order){
          for(let data of val){
            if(x == data.Size_Code){
              array.push(data);
            }
          }
        }
        this.datas.push(array);
      }
      );
      console.log("Data",this.datas);
      this.setcatalogueimages();
    }

    setcatalogueimages(){
      for(let x of this.datas){
        this.catalogueimages.push(x[0].catalogue_imgs);
      }
      this.mainimage = this.catalogueimages[0][0];
      this.productImages = this.catalogueimages[0];
      console.log("Catalogue_Images",this.catalogueimages);
    }

    getImage(item,i){
      this.mainimage = item;

    }
    getSum(data){
      var sum = 0;
      for(let x of data){
        sum = sum + x.quantity;
      }
      return sum;
    }
}
