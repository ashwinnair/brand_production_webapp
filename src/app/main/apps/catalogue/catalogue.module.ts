import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ImageZoomModule } from 'angular2-image-zoom';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatStepperModule,
  MatListModule,
  MatCheckboxModule,
  MatRadioModule,
  MatChipsModule,
  MatBadgeModule,
  MatSlideToggleModule,
  MatCardModule
} from '@angular/material';

import { CatalogueComponent } from './catalogue/catalogue.component';
import { CatalogueDetailsComponent } from './catalogue-details/catalogue-details.component';
import { UploadCatalogueComponent } from './upload-catalogue/upload-catalogue.component';
import { LinesheetsComponent } from './linesheets/linesheets.component';
import { ManageInventoryComponent } from './manage-inventory/manage-inventory.component';
import { CartComponent } from './cart/cart.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AddtolinesheetComponent } from './addtolinesheet/addtolinesheet.component';




const routes: Routes = [
  {
    path: 'list/:id',
    component: CatalogueComponent
  },
  {
    path: ':vendorcode/:stylecode',
    component: CatalogueDetailsComponent
  },
  {
    path: 'upload',
    component: UploadCatalogueComponent
  },
  {
    path: 'cart',
    component: AddtolinesheetComponent
  },
  {
    path: 'manage-inventory',
    component: ManageInventoryComponent
  },
  {
    path: 'linesheets',
    component: LinesheetsComponent
  },
  {
    path: 'linesheets/createlinesheet/:linesheetid',
    component: AddtolinesheetComponent
  }

];

@NgModule({
  declarations: [
    CatalogueComponent,
    CatalogueDetailsComponent,
    UploadCatalogueComponent,
    ManageInventoryComponent,
    LinesheetsComponent,
    CartComponent,
    SidebarComponent,
    AddtolinesheetComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatListModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatRadioModule,
    FuseSharedModule,
    FuseSidebarModule,
    ImageZoomModule,
    MatChipsModule,
    MatBadgeModule,
    MatSlideToggleModule,
    MatCardModule,
    NgxSmartModalModule.forRoot(),
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    })
  ]
})

export class CatalogueModule {
}
