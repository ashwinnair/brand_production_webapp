import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { CustomerService } from '../customer.service';

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {

  BrandData : any;
  BrandID : any;
  CartID : any;
  retailerID: any;
  ConnectionID: any;
  SearchResult :any =[];
  linesheetResult:any =[];

  constructor(
    private http: HttpClient,
    private customer: CustomerService
  ) {

    this.BrandData = JSON.parse(localStorage.getItem('userBrand'));
    this.BrandID = this.BrandData.brand_id;
    this.ConnectionID = this.BrandData._id;
    this.CartID = localStorage.getItem('CartID');
    this.retailerID = JSON.parse(localStorage.getItem('UserData'));
  }


  getCatalogueByFilter(data): Observable<any>{

    return this.http.get<any>(this.customer.Baseurl + 'catalogue/cataloguelist/'+ this.BrandID + '/' + data );

  }

  getCatalogueVerticalByFilter(data): Observable<any>{

    var removeSpace = data.replace(/\s+/g,'');

    return this.http.get<any>(this.customer.Baseurl + 'catalogue/cataloguelistbyvertical/' + removeSpace );

  }

  getCatalogueCategoriesByFilter(data): Observable<any>{

    return this.http.get<any>(this.customer.Baseurl + 'catalogue/cataloguelistby/' + this.BrandID + '/' + data );

  }

  getCatalogueData(): Observable<any>{
    return this.http.get<any>(this.customer.Baseurl + 'catalogue/cataloguelistbybrand/' + this.BrandID);
  }

  getCatalogueDiscountData(from, to): Observable<any>{
    return this.http.get<any>(this.customer.Baseurl + 'catalogue/cataloguelistbydiscount/' + from + '/' + to);
  }

  getCataloguePriceData(from, to): Observable<any>{
    return this.http.get<any>(this.customer.Baseurl + 'catalogue/cataloguelistbyprice/' + from + '/' + to);
  }

  getCatalogueFilterData(data): Observable<any>{
    var newUrl = '';
    for(var i=0; i<data.length; i++){
      var FilterUrl = data[i].replace(/\s+/g,'');

      // console.log(FilterUrl);
      newUrl += FilterUrl+"/";
    }
    console.log(newUrl);
    return this.http.get<any>(this.customer.Baseurl + 'catalogue/cataloguelistbytags/' + newUrl);
  }


  getColorsData(color_code, style_code): Observable<any>{

    return this.http.get<any>(this.customer.Baseurl + 'catalogue/catalogeDetailbySCndCC/' +  color_code + '/' + style_code.Style_code);

  }


  // product count api call

  getCategorycount(): Observable<any>{
    console.log();
    return this.http.get<any>(this.customer.Baseurl + 'catalogue/categorycount/' + this.BrandID)

  }

  getSegmentcount(): Observable<any>{
    console.log();
    return this.http.get<any>(this.customer.Baseurl + 'catalogue/segmentcount/' + this.BrandID)

  }

  getVerticalcount(): Observable<any>{
    console.log();
    return this.http.get<any>(this.customer.Baseurl + 'catalogue/verticalcount/' + this.BrandID)

  }


  getCartData(): Observable<any>{

    return this.http.get<any>(this.customer.Baseurl + 'cart/listingofcart/' + this.CartID );

  }

  getcataloguesearch(brandid,body){
    console.log("Catalogue Updated");
    this.http.post(this.customer.AdminBaseurl + 'catalogue/search/'+brandid,body).subscribe(response => {
      this.setcatalogue(response);
      console.log(response);
    })
  }
  setcatalogue(response){
    this.SearchResult = response.data;
  }

  getProducts(brandid,body): Observable<any>{
    console.log("Catalogue Updated");
    return this.http.post(this.customer.AdminBaseurl + 'catalogue/search/'+brandid,body)
  }


  getFilters(id,body):Observable<any>{
    return this.http.post<any>(this.customer.AdminBaseurl + 'catalogue/filters/' + id + '/true',body);
  }

  sendlinesheet(body):Observable<any>{
    return this.http.post<any>(this.customer.AdminBaseurl +'lineSheetCatalogue/createLineSheetCatalogue',body);
  }
  deletelinesheet(body):Observable<any>{
    return this.http.post<any>(this.customer.AdminBaseurl +'lineSheetCatalogue/deleteLinesheetCatalogue',body);
  }








}
