import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerService } from '../../customer.service';
import { EventEmiterService } from 'app/event.emmiter.service';
import { CatalogueService } from '../catalogue.service';

import { Router,ActivatedRoute } from '@angular/router';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';


import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {

  defaultImage = 'https://www.turfgrasssod.org/wp-content/themes/linstar/assets/images/default.jpg';
  counter : number = 0;
  calalogueList: any;
  BrandData : any;
  BrandID : any;
  colorGradesGroup: any[] = [];
  Bid:any;
  filterbody:any = {} ;
  keys = [];
  FilterData:any;
  filter = false;
  filterform:FormGroup;
  checkbox:any;
  filtercheckbox:any= [];
  filtertag:any = [];
  filterkeys:any = [];
  orderedfilter:any =[];
  removable = true;
  selectable = true;
  BrandName:String = '';

  constructor(
    private http: HttpClient,
    private customer: CustomerService,
    private router: Router,
    private route: ActivatedRoute,
    public api: CatalogueService,
    public EventEmiter: EventEmiterService,
    public fb:FormBuilder,
    private _fuseSidebarService: FuseSidebarService
  ) {

    this.filterform = fb.group({
        checkbox : fb.array([
          this.createcheckbox()
        ])
    })

  }
  createcheckbox(){
    return this.fb.group({
    checkbox_filter: false
  });
  }

  addcheckbox(): void {
  this.checkbox = this.filterform.get('checkbox') as FormArray;
  this.checkbox.push(this.createcheckbox());
}

  onGetFilteredList(data) {
    console.log(data);
    this.calalogueList = data;
  }

  ngOnInit() {

    // this.BrandData = JSON.parse(localStorage.getItem('BrandData'));
    // this.BrandID = this.BrandData.brand_id;

    let vendorCode = this.route.snapshot.paramMap.get('id');

    this.http.get(this.customer.AdminBaseurl + 'brand/brandDetailsByVendorCode/' + vendorCode )
      .subscribe(
        response => {
          this.setBrandData(response);
        });
  }
  setBrandData(response){
    if(response.status){
      this.Bid = response.data._id;
      this.BrandName = response.data.brand_name;
      this.getCatalogueData();
    }

  }

  getCatalogueData(){
    this.http.get(this.customer.AdminBaseurl + 'catalogue/cataloguelistbybrands/' + this.Bid)
      .subscribe(
        response => {
          this.setcataloguedata(response);
        },
        response => {
          alert(response);
        });

      this.getFilters({});
  }
  setcataloguedata(response){
    this.api.SearchResult = response.data;
  }

  getCatalogueDetails(calalogue): void {
    localStorage.setItem('catalogueData', JSON.stringify(calalogue));
    this.EventEmiter.catalogueDetails(calalogue.data[0]._id);
    localStorage.setItem('catalogue_id', calalogue.data[0]._id);

    let vendorCode = this.route.snapshot.paramMap.get('id');
    let url = '/apps/catalogue/'+ vendorCode+'/'+ calalogue._id.stylecode;
    // let url = '/apps/catalogue/'+ calalogue.data[0].brandname+'/'+ calalogue._id.stylecode;
    // window.open(url);
        console.log(url);
    this.router.navigateByUrl(url);
  }

  filters:any =[];

  getFilters(filter){
    const body = filter;
    this.filters=[];
    this.api.getFilters(this.Bid,body).subscribe(response => {
      this.FilterData = response.data;

          var order =
          ["portfolio","segment","department","vertical","category","brick","occasion","sleeve","neck","collar",
          "pattern","fabric","fit","material","composition","weave","technique","length","hemline","rise","Size_Code","Color_code"];

      let filterkeys = (Object.keys(this.FilterData));
      let index = 0;
      for(let count = 0; count< filterkeys.length; count++){
      if(this.FilterData[filterkeys[count]].length >0)
       {
           this.filters.push({
           index: index,
           filtername: filterkeys[count],
           filters: this.FilterData[filterkeys[count]]
         });
         index = index+1;
      }
      }
      var orderedarray =[];
      for(let x of order){
        for(let y of this.filters){
          if(x == y.filtername){
            orderedarray.push(y);
          }
        }
      }

    this.orderedfilter = orderedarray;
      this.setngmodel();
    });
}


  filtertags(filterbody){
    this.filtertag= [];
    let filterkeys = (Object.keys(this.filterbody));
    let index = 0;
    for(let count = 0; count< filterkeys.length; count++){
    if(this.filterbody[filterkeys[count]].length >0)
     {
         this.filtertag.push({
         index: index,
         filtername: filterkeys[count],
         filters: this.filterbody[filterkeys[count]]
       });
       index = index+1;
    }
    }
  }

setngmodel(){
  for(let i=0;i< this.orderedfilter.length; i++){
    this.filterkeys.splice(i,0,this.orderedfilter[i].filtername);
  }

  for(let i = 0 ; i<this.orderedfilter.length; i++){
    var index = 0;
    for(let x of this.orderedfilter[i].filters){
      this.filtercheckbox[x._id]= {value:false,name:''+i+index};
      index++;
    }
  }
  this.setcheckboxvalue();
}

setcheckboxvalue(){
  var keys= Object.keys(this.filterbody);
  console.log("filterbody Keys", keys);
  for(let x of keys){
    for(let y of this.filterbody[x]){
      this.filtercheckbox[y].value = true;
    }
  }
  console.log(this.filtercheckbox);
}
  createfilterarray(key,value){
    if(this.keys.indexOf(key) == -1 ){
      this.keys.push(key);
    }
    else {
      console.log(this.keys.indexOf(key));
    }

    console.log(this.keys);
    for(let i = 0; i < this.keys.length; i++){
    this.filterbody[this.keys[i]] = [];
    }
  }

  onFilterChange(){
    console.log(this.filterform.value.filters);
  }

  showmevalue(event,key,value,colorName){

        if(event == true){
          if(this.filterbody[key] == undefined){
            this.filterbody[key] = [value];

            //// To color name show on chip
            if(key == 'Color_code')
            {
              this.filterbody[key]['selectedColorName'] = colorName;
            }
          }
          else if(this.filterbody[key].length > 0){
            if(this.filterbody[key].indexOf(value) == -1){
            this.filterbody[key].splice(this.filterbody[key].indexOf(value),0,value);
            }
            else if(this.filterbody[key].indexOf(value) > 0){
              this.filterbody[key].splice(this.filterbody[key].indexOf(value),1,value);
            }
          }
        }
        else if(event == false){
          this.filterbody[key].splice(this.filterbody[key].indexOf(value),1);
          delete this.filterbody[key];
        }
        this.api.getcataloguesearch(this.Bid,this.filterbody);
        this.filtertags(this.filterbody);
        this.getFilters(this.filterbody);
  }

  removetag(filter,value){
    this.filterbody[filter.filtername].splice(this.filterbody[filter.filtername].indexOf(value),1);
    if(this.filterbody[filter.filtername].length == 0){
        delete this.filterbody[filter.filtername];
    }
    this.filtercheckbox[value].value = false;
    this.api.getcataloguesearch(this.Bid,this.filterbody);
    this.getFilters(this.filterbody);
  }

  toggleSidebar(name): void
{
    this._fuseSidebarService.getSidebar(name).toggleOpen();
}



}
