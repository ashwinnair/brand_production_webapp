import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinesheetsComponent } from './linesheets.component';

describe('LinesheetsComponent', () => {
  let component: LinesheetsComponent;
  let fixture: ComponentFixture<LinesheetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinesheetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinesheetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
