import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../../customer.service';
import { HttpClient } from '@angular/common/http';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

declare var $;

@Component({
  selector: 'app-linesheets',
  templateUrl: './linesheets.component.html',
  styleUrls: ['./linesheets.component.scss']
})
export class LinesheetsComponent implements OnInit {

  data:any;
  allBrands:any;
  linesheetform:FormGroup;
  ProfilePic:any;
  allLinesheets :any = {
    data : []
  }

  constructor(
    private router: Router,
    private customer: CustomerService,
    public ngxSmartModalService: NgxSmartModalService,
    public fb:FormBuilder,
    public http: HttpClient
  ) {

    this.linesheetform = fb.group({
      'linesheetname':[''],
      'brandname':[''],
      'profilepic_name':[{value:'',disabled:true}],
      'profilepic':['']
    });


  }

  getLinesheets(){
    this.http.get(this.customer.AdminBaseurl + 'lineSheet/getLinesheetbycompany/'+this.data.companyId).subscribe(
      response => {
        this.allLinesheets = response;
      }
    )
  }

  ngOnInit() {
    this.data = JSON.parse(localStorage.getItem('BrandInfo'));
    this.http.get(this.customer.AdminBaseurl + 'brand/brandsByCompany/'+this.data.companyId)
      .subscribe(
        response => {
          this.setdata(response);
          this.getLinesheets();
        },
        response => {
          alert(response);
        });
  }
  setdata(response){
    this.allBrands = response.data;
  }
  uploadpp(event){
      if(event.target.files.length > 0) {
        let file = event.target.files[0];
        console.log(file);
        this.linesheetform.get('profilepic_name').setValue(file.name);
        this.ProfilePic = file;

      }
  }


  Createlinesheet(){
    const body = new FormData();
    body.append('name',this.linesheetform.get('linesheetname').value);
    body.append('brandId',this.linesheetform.get('brandname').value);
    body.append('companyId',this.data.companyId);
    body.append('logo',this.ProfilePic);
    this.http.post(this.customer.AdminBaseurl + 'lineSheet/createLineSheet',body).subscribe(response =>{
      this.linesheetform.reset();
      this.ngxSmartModalService.getModal('addlinesheet').close();
      this.getLinesheets();
    });
  }

  gotolinesheet(id){
    console.log(id);
    let url = '/apps/catalogue/linesheets/createlinesheet/'+ id;
    this.router.navigateByUrl(url);
  }

  activatelinesheet(brand){
    console.log(brand.publish);
    var value;
    if(brand.publish){
      value = false;
    }
    else{
      value = true;
    }
    const body = new FormData();
    body.append('publish',value);
    this.http.patch(this.customer.AdminBaseurl+ 'lineSheet/updateLinesheet/'+brand._id,body)
    .subscribe(
      response => {
        console.log(response);
        this.getLinesheets();
      }
    )
  }

}
