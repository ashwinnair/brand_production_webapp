import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadCatalogueComponent } from './upload-catalogue.component';

describe('UploadCatalogueComponent', () => {
  let component: UploadCatalogueComponent;
  let fixture: ComponentFixture<UploadCatalogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadCatalogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadCatalogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
