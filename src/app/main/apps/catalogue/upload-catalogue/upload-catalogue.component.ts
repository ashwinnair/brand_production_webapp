import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { HttpClient, HttpResponse, HttpRequest, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Subscription, of, observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';


import { FuseConfigService } from '@fuse/services/config.service';
import { CustomerService } from '../../customer.service';


@Component({
  selector: 'app-upload-catalogue',
  templateUrl: './upload-catalogue.component.html',
  styleUrls: ['./upload-catalogue.component.scss']
})
export class UploadCatalogueComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private customer: CustomerService) { }

  selectedFile: File = null;
  selectedFileName: any;

  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
    this.selectedFileName = this.selectedFile.name;

  }

  onUpload() {

    const uploadData = new FormData();
    // uploadData.append('name', this.selectedFile.name);
    uploadData.append('filename', this.selectedFile);
    
    console.log(uploadData);

    this.http.post(this.customer.Baseurl + 'image/upload/file/catalogue', uploadData, {
      reportProgress: true,
      observe: 'events',
    })
    .subscribe(event => {

      if(event.type === HttpEventType.UploadProgress) {
        console.log(Math.round(event.loaded / event.total * 100) + '%');
      } else if(event.type === HttpEventType.Response) {
        console.log(event);
      }

    })
  }


  ngOnInit() {

      }

}
