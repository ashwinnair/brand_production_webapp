import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../account/account.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  pagename = 'details';
  data:any;
  dataID:any;

  constructor(public ngxSmartModalService: NgxSmartModalService,fb: FormBuilder, public api :AccountService) {

    this.dataID = JSON.parse(localStorage.getItem('userBrand'));
    console.log(this.dataID.userData.companyId);

   }

  ngOnInit() {
    this.getCompanyInfo();
  }

  getCompanyInfo(){
    this.api.getCompanyInfo(this.dataID.userData.companyId).subscribe(response => {
      if(response.status){
        this.setdata(response);
      }

    })
  }

  setdata(response){
    this.data = response.data;
  }



opendetails(){
  this.pagename = 'details';
}
openTeams(){
    this.pagename = 'teams';
}
openWarehouses(){
  this.pagename = 'warehouses';
}
openBanks(){
  this.pagename = 'banks';
}

}
