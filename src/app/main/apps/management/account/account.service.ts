import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { CustomerService } from '../../customer.service';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(public http:HttpClient, public customer:CustomerService ) { }


  getdetails(id):Observable<any>{
    return this.http.get(''+id);
  }


  getTeambyCompany(companyId):Observable<any>{
      return this.http.get(this.customer.AdminBaseurl + 'team/auth/teamsByCompany/'+companyId);
    }

  addteaminfo(data,profilepic,companyId,email_send):Observable<any>{
    const body = new FormData();
    body.append('fname',data.fname);
    body.append('lname',data.lname);
    body.append('role',data.role);
    body.append('mobileNo',data.mobileNo);
    body.append('landlineNo',data.landlineNo);
    body.append('designation',data.designation);
    body.append('email',data.email);
    body.append('profilepic',profilepic);
    body.append('password',data.password);
    body.append('confirm_email_sent',email_send);
    body.append('companyId',companyId);
    body.append('brandId','');

    return this.http.post(this.customer.AdminBaseurl + 'team/auth/register',body)
  }

  addwarehouses(companyId,data,adp,gst) :Observable<any>{
    const body = new FormData();
    body.append('name',data.name);
    body.append('type',data.type);
    body.append('companyId',companyId);
    body.append('address',data.address);
    body.append('city',data.city);
    body.append('state',data.state);
    body.append('pincode',data.pincode);
    body.append('mobile',data.mobile);
    body.append('landline',data.landline);
    body.append('address_proof_file',adp);
    body.append('GST_number',data.GST_number);
    body.append('GST_file',gst);
    body.append('status','1');
    return this.http.post(this.customer.AdminBaseurl + 'warehouse/createWarehouse',body)
  }

  getWarehousebyCompany(companyId):Observable<any>{
      return this.http.get(this.customer.AdminBaseurl + 'warehouse/warehousesByCompany/'+companyId);
    }


    addbanks(data,companyId) :Observable<any>{
      const body = new FormData();
      body.append('benificiary_name',data.benificiary_name);
      body.append('ifsc_code',data.ifsc_code);
      body.append('bank_name',data.bank_name);
      body.append('branch_name',data.branch_name);
      body.append('account_type',data.account_type);
      body.append('account_number',data.account_number);
      body.append('swift_code',data.swift_code);
      body.append('companyId',companyId);
      body.append('status','1');
      body.append('brandId','');
      return this.http.post(this.customer.AdminBaseurl + 'bank/addBank',body)
    }

    getBanksbycompany(companyId):Observable<any>{
        return this.http.get(this.customer.AdminBaseurl + 'bank/banksByCompany/'+companyId);
      }

    getCompanyInfo(companyId):Observable<any>{
      return this.http.get(this.customer.AdminBaseurl + 'company/companyDetailsById/'+companyId);
    }

    updateTeam(id,body):Observable<any>{
      return this.http.patch(this.customer.AdminBaseurl + 'team/auth/updateTeam/'+id,body);
    }

    updateWarehouse(id,body):Observable<any>{
      return this.http.patch(this.customer.AdminBaseurl + 'warehouse/updateWarehouse/'+id,body);
    }

    updateBank(id,body):Observable<any>{
      return this.http.patch(this.customer.AdminBaseurl + 'bank/updateBank/'+id,body);
    }


    deleteTeam(teamId):Observable<any>{
      return this.http.delete(this.customer.AdminBaseurl + 'team/auth/deleteTeam/'+teamId);
    }

    deleteWarehouse(id):Observable<any>{
      return this.http.delete(this.customer.AdminBaseurl + 'warehouse/deleteWarehouse/'+id);
    }

    deleteBank(id):Observable<any>{
      return this.http.delete(this.customer.AdminBaseurl + 'bank/deleteBank/'+id);
    }


    getmasterstate():Observable<any>{
      return this.http.get(this.customer.AdminBaseurl + 'merchandiseMaster/allStates');
    }

}
