import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { CustomerService } from '../../../customer.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-add-warehouse',
  templateUrl: './add-warehouse.component.html',
  styleUrls: ['./add-warehouse.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddWarehouseComponent {
  action: string;
  operations_team_details: FormGroup;
  dialogTitle: string;

  /**
   * Constructor
   *
   * @param {MatDialogRef<AddWarehouseComponent>} matDialogRef
   * @param _data
   * @param {FormBuilder} _formBuilder
   */
  constructor(
      public matDialogRef: MatDialogRef<AddWarehouseComponent>,
      @Inject(MAT_DIALOG_DATA) private _data: any,
      private _formBuilder: FormBuilder,
      private customer: CustomerService,
      private http: HttpClient,
  )
  {
      // Set the defaults
      this.action = _data.action;

      if ( this.action === 'edit' )
      {
          this.dialogTitle = 'Edit Contact';
          // this.contact = _data.contact;
      }
      else
      {
          this.dialogTitle = 'Add New Team';
          // this.contact = new Contact({});
      }

      this.operations_team_details = this.createTeamForm();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Create contact form
   *
   * @returns {FormGroup}
   */
  createTeamForm(): FormGroup
  {
      return this._formBuilder.group({
        firstname    : ['', Validators.required],
        middlename: ['',],
        lastname : ['', Validators.required],
        designation: ['', Validators.required],
        email_id   : ['',  [Validators.required, Validators.email]],
        mobile_number   : ['', Validators.required],
        landline_number   : ['', Validators.required]
      });
  }


  


}