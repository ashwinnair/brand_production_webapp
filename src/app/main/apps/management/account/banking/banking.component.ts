import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../../account/account.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-banking',
  templateUrl: './banking.component.html',
  styleUrls: ['./banking.component.scss']
})
export class BankingComponent implements OnInit {

      data:any;
      form:any
      bankData:any;
      compnayId:any;

      selectedBankId:any;

    constructor(fb:FormBuilder,public api:AccountService,public ngxSmartModalService: NgxSmartModalService) {
      this.data = JSON.parse(localStorage.getItem('userBrand'));
      console.log(this.data);
      this.compnayId = this.data.userData.companyId;

      this.form = fb.group({
        'benificiary_name':[''],
        'ifsc_code':[''],
        'bank_name':[''],
        'branch_name':[''],
        'account_type':[''],
        'account_number':[''],
        'swift_code':[''],
        'companyId':[''],
        'status':[''],
        'brandId':['']
      })
      this.getBanks();
    }

    ngOnInit() {
    }

    openAddBank(){
      this.form.reset();
      this.ngxSmartModalService.getModal('addbank').open();
    }


    addBank(){
      this.api.addbanks(this.form.value,this.compnayId).subscribe(response => {
          this.ngxSmartModalService.getModal('addbank').close();
          this.form.reset();
          this.getBanks();
      })
    }


    getBanks(){
      this.api.getBanksbycompany(this.compnayId).subscribe(response => {
        if(response.status){
          this.setteamdata(response);
        }

      })
    }

    setteamdata(response){
      this.bankData = response.data;
    }

    editBank(data){

      console.log("Data comes:",data);

      this.selectedBankId = data._id;

      this.form.reset();

      this.ngxSmartModalService.getModal('editbank').open();

      this.form.patchValue(data);

    }
    updateBank(){

      let data = this.form.value;

      const body = new FormData();

      body.append('benificiary_name',data.benificiary_name);
      body.append('account_number',data.account_number);
      body.append('ifsc_code',data.ifsc_code);
      body.append('bank_name',data.bank_name);
      body.append('branch_name',data.branch_name);
      body.append('account_type',data.account_type);
      body.append('swift_code',data.swift_code);

      this.api.updateBank(this.selectedBankId,body).subscribe(response => {
        this.form.reset();
        this.ngxSmartModalService.getModal('editbank').close();
        this.getBanks();
      })

    }


    statuschange(x){

      var status = '0';
      if(x.status == '0'){
        status = '1';
      }
      else if(x.status == '1'){
        status = '0';
      }
      const body = new FormData();
      body.append('status',status);
        this.api.updateBank(x._id,body).subscribe(response => {
          this.getBanks();
        })
    }
    deleteBank(bankId){

      this.api.deleteBank(bankId).subscribe(response=>{

          this.getBanks();

      })

    }

}
