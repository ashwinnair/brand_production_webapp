import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../account/account.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  dataID:any;
  data:any;

  constructor(public api:AccountService) { }

  ngOnInit() {
    this.dataID = JSON.parse(localStorage.getItem('userBrand'));
    console.log(this.dataID);
    this.getdata();

  }

  getdata(){
    this.api.getCompanyInfo(this.dataID.userData.companyId).subscribe(response=>{
      if(response.status){
        this.setdata(response);
      }

    })
  }

  setdata(response){
    this.data = response.data;
  }



}
