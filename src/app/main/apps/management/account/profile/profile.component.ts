import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  resetPasswordForm: FormGroup;

  // Private
  private _unsubscribeAll: Subject<any>;


  constructor(
    private _formBuilder: FormBuilder,
  ) { 

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {

    this.resetPasswordForm = this._formBuilder.group({
        token          : ['', Validators.required],
        password       : ['', Validators.required],
        passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
    });

    this.resetPasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.resetPasswordForm.get('passwordConfirm').updateValueAndValidity();
            });


  }

  tryResetPassword() {

    console.log(this.resetPasswordForm.value);
    // this.api.ResetPassword(
    //     this.resetPasswordForm.value.token,
    //     this.resetPasswordForm.value.password
    // )
    //     .subscribe(
    //         response => {
    //             if (response) {
    //                 this.router.navigateByUrl('apps/auth/login');
    //             }
    //         },
    //         response => {
    //             alert(response.error.error);
    //         });
}



}

export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if ( !control.parent || !control )
  {
      return null;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if ( !password || !passwordConfirm )
  {
      return null;
  }

  if ( passwordConfirm.value === '' )
  {
      return null;
  }

  if ( password.value === passwordConfirm.value )
  {
      return null;
  }

  return {'passwordsNotMatching': true};
};
