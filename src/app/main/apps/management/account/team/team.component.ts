import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../../account/account.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  isChecked : boolean = true;
  addteam : FormGroup;
  data:any;
  ProfilePic:any;
  teamdata:any;
  compnayId:any;

  selectedTeamId:any;

  constructor(fb:FormBuilder,public api:AccountService,public ngxSmartModalService: NgxSmartModalService) {
    this.data = JSON.parse(localStorage.getItem('userBrand'));
    console.log(this.data);
    this.compnayId = this.data.userData.companyId;
    this.addteam = fb.group({
        'fname':[''],
        'lname':[''],
        'role':['Admin'],
        'mobileNo':[''],
        'landlineNo':[''],
        'designation':[''],
        'email':[''],
        'profilepic':[''],
        'profilepic_name':[{value:'',disabled:true}],
        'password':[''],
        'confirm_email_sent':[0]
    })
    this.getTeams();
   }

  ngOnInit() {
  }

  uploadpp(event){
      if(event.target.files.length > 0) {
        let file = event.target.files[0];
        console.log(file);
        this.addteam.get('profilepic_name').setValue(file.name);
        this.ProfilePic = file;

      }
  }

  AddTeamDetails(){

    console.log(this.addteam.get('confirm_email_sent').value);
    var email_send = 0;
    if(this.addteam.get('confirm_email_sent').value == true){
       email_send = 1;
    }
    this.api.addteaminfo(this.addteam.value,this.ProfilePic,this.compnayId,email_send).subscribe(response => {
        this.ngxSmartModalService.getModal('addopsteam').close();
        this.addteam.reset();
        this.getTeams();
    })
  }

  getTeams(){
    this.api.getTeambyCompany(this.compnayId).subscribe(response => {
      if(response.status){
        this.setteamdata(response);
      }

    })
  }

  setteamdata(response){
    this.teamdata = response.data;
  }

  openAddOpsTeam(){
    this.addteam.reset();
    this.ngxSmartModalService.getModal('addopsteam').open();

  }
  editTeam(data){
    console.log("Data comes:",data);

    this.selectedTeamId = data._id;

    this.addteam.reset();
    this.ngxSmartModalService.getModal('editopsteam').open();


    this.addteam.controls['fname'].setValue(data.fname);
    this.addteam.controls['lname'].setValue(data.lname);
    this.addteam.controls['role'].setValue(data.role);
    this.addteam.controls['mobileNo'].setValue(data.mobileNo);
    this.addteam.controls['landlineNo'].setValue(data.landlineNo);
    this.addteam.controls['designation'].setValue(data.designation);
    this.addteam.controls['email'].setValue(data.email);
    this.addteam.controls['confirm_email_sent'].setValue(data.confirm_email_sent);

    this.ProfilePic = data.profilepic;

  }

  updateTeamDetails(){

    let data = this.addteam.value;

    const body = new FormData();

    body.append('fname',data.fname);
    body.append('lname',data.lname);
    body.append('role',data.role);
    body.append('mobileNo',data.mobileNo);
    body.append('landlineNo',data.landlineNo);
    body.append('designation',data.designation);
    body.append('email',data.email);
    body.append('profilepic',this.ProfilePic);

    if(data.password)
    {
      body.append('password',data.password);
    }

    body.append('confirm_email_sent',data.confirm_email_sent);

    this.api.updateTeam(this.selectedTeamId,body).subscribe(response => {

      this.addteam.reset();
      this.ngxSmartModalService.getModal('editopsteam').close();
      this.getTeams();
    })

  }

  statuschange(x){
    var status = '0';
    if(x.status == '0'){
      status = '1';
    }
    else if(x.status == '1'){
      status = '0';
    }
    const body = new FormData();
    body.append('status',status);
      this.api.updateTeam(x._id,body).subscribe(response => {
        this.getTeams();
      })
  }

  deleteTeam(teamId){
    this.api.deleteTeam(teamId).subscribe(response=>{

        this.getTeams();

    })

  }

}
