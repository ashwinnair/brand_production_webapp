import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../../account/account.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent implements OnInit {
  data:any;
  form:FormGroup;
  adp:any;
  gst:any;
  warehouse:any;
  compnayId:any;

  states:any = {
    data:[]
  }

  selectedWarehouseId:any;

constructor(fb:FormBuilder,public api:AccountService,public ngxSmartModalService: NgxSmartModalService) {

  this.data = JSON.parse(localStorage.getItem('userBrand'));
  console.log(this.data);
  this.compnayId = this.data.userData.companyId;

  this.form = fb.group({
      'name':[''],
      'type':[''],
      'address':[''],
      'city':[''],
      'state':[''],
      'pincode':[''],
      'mobile':[''],
      'landline':[''],
      'GST_number':[''],
      'address_proof_file_name':[''],
      'address_proof_file':[''],
      'GST_file_name':[''],
      'GST_file':['']
  })

}

ngOnInit() {

  this.getwarehouseinfo();
  this.getstates();
}

getstates(){
  this.api.getmasterstate().subscribe(response => {
    if(response.status){
      this.states = response.data;
    }

  })
}

uploadadp(event){
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      console.log(file);
      this.form.get('address_proof_file_name').setValue(file.name);
      this.adp = file;

    }
}
uploadpp(event){
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      console.log(file);
      this.form.get('GST_file_name').setValue(file.name);
      this.gst = file;

    }
}
openAddWarehouse()
{
  this.form.reset();
  this.ngxSmartModalService.getModal('addwarehouse').open();
}

addWarehouse(){
  this.api.addwarehouses(this.compnayId,this.form.value,this.adp,this.gst).subscribe(response=>{
    this.form.reset();
    this.ngxSmartModalService.getModal('addwarehouse').close();
    this.getwarehouseinfo();

  })
}

getwarehouseinfo(){
  this.api.getWarehousebyCompany(this.compnayId).subscribe(response => {
    if(response.status){
      this.setwarehousedata(response);
    }

  })
}

setwarehousedata(response){
  this.warehouse = response.data;
  console.log(this.warehouse);
}

editWarehouse(data){
  console.log("Data comes:",data);

  this.selectedWarehouseId = data._id;

  this.form.reset();

  this.ngxSmartModalService.getModal('editwarehouse').open();


  this.form.controls['name'].setValue(data.name);
  this.form.controls['type'].setValue(data.type);
  this.form.controls['address'].setValue(data.address);
  this.form.controls['city'].setValue(data.city);
  this.form.controls['state'].setValue(data.state);
  this.form.controls['pincode'].setValue(data.pincode);
  this.form.controls['mobile'].setValue(data.mobile);
  this.form.controls['landline'].setValue(data.landline);
  this.form.controls['GST_number'].setValue(data.GST_number);

  this.adp = data.address_proof_file;
  this.gst = data.GST_file;

}

updateWarehouse(){
  let data = this.form.value;

  const body = new FormData();

  body.append('name',data.name);
  body.append('type',data.type);
  body.append('address',data.address);
  body.append('city',data.city);
  body.append('state',data.state);
  body.append('pincode',data.pincode);
  body.append('mobile',data.mobile);
  body.append('landline',data.landline);
  body.append('GST_number',data.GST_number);
  body.append('address_proof_file',this.adp);
  body.append('GST_file',this.gst);

  this.api.updateWarehouse(this.selectedWarehouseId,body).subscribe(response => {

    this.form.reset();
    this.ngxSmartModalService.getModal('editwarehouse').close();
    this.getwarehouseinfo();
    })

}

changeStatus(x){

  var status = '0';
  if(x.status == '0'){
    status = '1';
  }
  else if(x.status == '1'){
    status = '0';
  }
  const body = new FormData();
  body.append('status',status);
    this.api.updateWarehouse(x._id,body).subscribe(response => {
      this.getwarehouseinfo();
    })

}

deleteWarehouse(warehouseId){

  this.api.deleteWarehouse(warehouseId).subscribe(response=>{

      this.getwarehouseinfo();

  })

}

}
