import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../../customer.service';

declare var $;

@Component({
  selector: 'app-add-distributor',
  templateUrl: './add-distributor.component.html',
  styleUrls: ['./add-distributor.component.scss']
})
export class AddDistributorComponent implements OnInit {

  dtOptions: any = {};

  @ViewChild('dataTable') table;
  dataTable:any;
  dtOption: any = {};

  constructor(
    private router: Router,
    private customer: CustomerService,
  ) { }

  ngOnInit() {

        this.dtOptions = {
          "ajax": {
            url: this.customer.Baseurl + 'order/brandOrderListing/5c876e6f89c1cc2b9ce9dfd4/pending/OR',
            type: 'GET'
          },
          columns: [
              {
                title: 'ID',
                data: 'brand_id'
              },
              {
                title: 'Distributor Name',
                data: 'retailer_id.company_details.company_name'
              },
              {
                title: 'Email',
                data: 'purchase_order_number'
              },
              {
                title: 'Phone',
                data: 'total_qty'
              },
              {
                title: 'Authorized Signatory',
                data: 'total_price'
              },
              {
                title: 'Assigned Brands',
                data: 'orderDate'
              },
              {
                title: 'Assigned Brands',
                data: 'orderDate'
              },
              {
                title: 'Action',
                render: function (data: any, type: any, full: any) {
                  return '<a class="view_details">Assign Brand</a><a class="view_details1">View Profile</a>';
                }
              }
            ],

            rowCallback: (row: Node, data: any[] | Object, index: number) => {
              const self = this;
              $('.view_details', row).unbind('click');
              $('.view_details', row).bind('click', () => {
                self.someClickHandler(data);
              });
              return row;
            },
          dom: 'Rt,Bfrtip',
          button:[
            'columnsToggle',
             'copy',
             'print',
             'excel','pdf'
          ]
        };
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable(this.dtOptions);
  }
  someClickHandler(data) {
  }
}
