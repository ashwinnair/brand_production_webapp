import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { AccountService } from '../account/account.service';
import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';
import { CustomerService } from '../../customer.service';

declare var $;

@Component({
  selector: 'app-add-retailer',
  templateUrl: './add-retailer.component.html',
  styleUrls: ['./add-retailer.component.scss']
})
export class AddRetailerComponent implements OnInit {

    addLead: FormGroup;
    dtOptions: any = {};
    @ViewChild('dataTable') table;
    dataTable:any;
    dtOption: any = {};
    id:any;
    teamMemberId:any;

    table1:any;




  constructor(fb:FormBuilder,public api:AccountService,public ngxSmartModalService: NgxSmartModalService, public customer:CustomerService, private http: HttpClient) {

    this.addLead = fb.group({
        'comapnyname':[''],
        'firstname':[''],
        'lastname':[''],
        'mobileNo':[''],
        'landlineNo':[''],
        'name':[''],
        'email':[''],
        'website':[''],
        'message':[''],
        'subject':['']
        });

  }

  ngOnInit() {

    const userDetails = JSON.parse(localStorage.getItem('userBrand'));
    this.teamMemberId = userDetails.userData._id;

    this.getAllLeads();

}

getAllLeads()
{
  this.dtOptions = {
    "ajax": {
      url: this.customer.AdminBaseurl + 'lead/leadsByTeamId/' + this.teamMemberId ,
      type: 'GET'
    },
    retrieve: true,
    destroy: true,
    columns: [
        {
          title: 'Company Name',
          data: 'company_name'
        },
        {
          title: 'Retail Chain Name',
          data: 'name'
        },
        {
            title: 'Name',
            render: function (data: any, type: any, full: any) {
              var name = full.fname +' ' + full.lname;
              return name
            }
        },
        {
            title: 'Mobile',
            data: 'mobile_no'
        },
        {
            title: 'Landline Number',
            data: 'landline_no'
        },
        {
            title: 'Email',
            data: 'email'
        },
        {
            title: 'Current Status',
            //data: 'status'
            render: function (data: any, type: any, full: any) {
              var status = '';

              switch(full.status) {
                case '0': {
                  //statements;
                  status = "Lead Initiated";
                  break;
                }
                case '1': {
                  //statements;
                  status = "Lead Suspended";
                  break;
                }
                default: {
                  //statements;
                  break;
                }
              }
              return status
            }
        },
        {
          title: 'Action',
          render: function (data: any, type: any, full: any) {
            return '<a class="delete_lead">Delete</a>';
          }
        }
      ],
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('.delete_lead', row).unbind('click');
        $('.delete_lead', row).bind('click', () => {
          self.deleteLead(data);
        });
        return row;
      },
    dom: 'Rt,Bfrtip',
    button:[
       'colvis'
    ]
  };
  this.dataTable = $(this.table.nativeElement);
  this.table1 = this.dataTable.DataTable(this.dtOptions);
}

deleteLead(data) {

  this.http.delete(this.customer.AdminBaseurl + 'lead/deleteLead/' + data._id)
  .subscribe(response => {
    this.table1.destroy();
    this.getAllLeads();
  })

}

AddLead(){

  const body = new FormData();

  body.append('company_name',this.addLead.get('comapnyname').value);
  body.append('fname',this.addLead.get('firstname').value);
  body.append('lname',this.addLead.get('lastname').value);
  body.append('mobile_no',this.addLead.get('mobileNo').value);
  body.append('landline_no',this.addLead.get('landlineNo').value);
  body.append('name',this.addLead.get('name').value);
  body.append('email',this.addLead.get('email').value);
  body.append('website',this.addLead.get('website').value);
  body.append('message',this.addLead.get('message').value);
  body.append('subject',this.addLead.get('subject').value);

  body.append('team_id',this.teamMemberId);

  this.http.post(this.customer.AdminBaseurl + 'lead/createLead',body)
  .subscribe(response => {
    this.table1.destroy();
    this.getAllLeads();
    this.ngxSmartModalService.getModal('addretailer').close();
  })

}

  }
