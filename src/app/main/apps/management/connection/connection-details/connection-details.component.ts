import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CustomerService } from '../../../customer.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-connection-details',
  templateUrl: './connection-details.component.html',
  styleUrls: ['./connection-details.component.scss']
})
export class ConnectionDetailsComponent implements OnInit {

  connectionId:any;

  connections:any = {
    data:{
      marginTerms:[]
    }
  }

  Departments:any = {
    data:[]
  }
  Segments:any = {
    data:[]
  }

  constructor(public http:HttpClient, public customer: CustomerService,private route: ActivatedRoute,public router:Router) {

    this.connectionId = this.route.snapshot.paramMap.get('connectionId');

    this.getConnectionDetails();
   }

  ngOnInit() {

    this.getDepartment();
    this.getSegment();
  }

  getConnectionDetails(){
    this.http.get(this.customer.AdminBaseurl + 'connection/connectionDetailsById/' + this.connectionId).subscribe(
      response => {
        this.connections = response;

        if(this.connections.data.connectionInitiatedBrandId){
          this.getDispatchLocationBrand(this.connections.data.connectionInitiatedBrandId._id);
        }

        if(this.connections.data.connectionInitiatedDistributorId){
          this.getDispatchLocationDistributor(this.connections.data.connectionInitiatedDistributorId._id);
        }

        // this.getDeliveryLocation(this.connections.data.connectionInitiatedRetailerId._id);
        // this.getDispatchLocation(this.connections.data.connectionInitiatedBrandId._id);
      }
    )
  }


  getDepartment(){
    this.http.get(this.customer.AdminBaseurl + 'merchandiseMaster/allDepartments').subscribe(
      response =>{
      this.Departments = response;
    });
  }
  getSegment(){
    this.http.get(this.customer.AdminBaseurl + 'merchandiseMaster/allSegments').subscribe(
      response =>{
      this.Segments = response;
    });
  }

  // deliverylocations:any = [];
  // getDeliveryLocation(id){
  //
  //   this.http.get(this.customer.AdminBaseurl + 'retailerDeliveryLocation/deliveryLocationsByRetailer/'+id)
  //   .subscribe(response => {
  //     this.setwarehouseretailer(response);
  //   });
  //
  // }
  //
  // retailerWarehouse:any = [];
  // setwarehouseretailer(response){
  //   this.retailerWarehouse = response.data;
  //   for(let x of response.data ){
  //
  //     this.deliverylocations.push({_id:x._id,name:x.name,type:x.type});
  //   }
  // }


  // deliverylocations:any = [];
  // getDeliveryLocation(id){
  //
  //   var warehouse;
  //   var stores;
  //   this.http.get(this.customer.AdminBaseurl + 'retailerWarehouse/warehousesByRetailer/'+id)
  //   .subscribe(response => {
  //     this.setwarehouseretailer(response);
  //   });
  //   this.http.get(this.customer.AdminBaseurl + 'store/storesByRetailer/'+id)
  //   .subscribe(response => {
  //     this.setstoreretailer(response);
  //   });
  //
  // }
  //
  // retailerWarehouse:any = [];
  // setwarehouseretailer(response){
  //   this.retailerWarehouse = response.data;
  //   for(let x of response.data ){
  //
  //     this.deliverylocations.push({_id:x._id,name:x.name,type:"warehouse"});
  //   }
  // }
  // retailerStore:any = [];
  // setstoreretailer(response){
  //   this.retailerStore = response.data;
  // for(let y of response.data){
  //   this.deliverylocations.push({ _id:y._id,name:y.store_name,type:"store"});
  // }
  // }


  dispatchlocationsBrand:any=[]
  getDispatchLocationBrand(id){
    this.http.get(this.customer.AdminBaseurl + 'warehouse/warehousesOfBrand/'+id)
    .subscribe(response => {
        this.setwarehousebrand(response);
    });

  }

  setwarehousebrand(response){
    for(let x of response.data ){
      // this.dispatchlocations.push(x);
       this.dispatchlocationsBrand.push({ _id:x._id,name:x.name});
    }
  }

  dispatchlocationsDistributor:any=[]
  getDispatchLocationDistributor(id){
    this.http.get(this.customer.AdminBaseurl + 'distributorWarehouse/warehousesByDistributor/'+id)
    .subscribe(response => {
        this.setwarehouseDistributor(response);
    });

  }

  setwarehouseDistributor(response){
    for(let x of response.data ){
      // this.dispatchlocations.push(x);
       this.dispatchlocationsDistributor.push({ _id:x._id,name:x.name});
    }
  }

  getdispatchwarehouseBrand(data){
    return this.dispatchlocationsBrand.filter(x => x._id === data).map(x => x.name);
  }

  getdispatchwarehouseDistributor(data){
    return this.dispatchlocationsDistributor.filter(x => x._id === data).map(x => x.name);
  }

  // getstore(data){
  //   return this.retailerStore.filter(x => x._id === data).map(x => x.store_name);
  // }

  // getretailerwarehouse(data){
  //   return this.retailerWarehouse.filter(x => x._id === data).map(x => x.name);
  // }

}
