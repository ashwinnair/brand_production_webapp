import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent implements OnInit {

  status:number = 0;
  activetab1:any = 'active';
  activetab2:any = '';
  activetab3:any = '';
  activetab4:any = '';

  constructor() { }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
  }

  changetoConnected(){
    this.status = 0;
    this.activetab1 = true;
    this.activetab2 = false;
    this.activetab3 = false;
    this.activetab4 = false;

  }
  changetoPWA(){
    this.status = 1;
    this.activetab1 = false;
    this.activetab2 = true;
    this.activetab3 = false;
    this.activetab4 = false;
  }
  changetoIS(){
    this.status = 2;
    this.activetab1 = false;
    this.activetab2 = false;
    this.activetab3 = true;
    this.activetab4 = false;
  }
  changetoIR(){
    this.status = 3;
    this.activetab1 = false;
    this.activetab2 = false;
    this.activetab3 = false;
    this.activetab4 = true;
  }

}
