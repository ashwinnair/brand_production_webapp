import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteRecievedComponent } from './invite-recieved.component';

describe('InviteRecievedComponent', () => {
  let component: InviteRecievedComponent;
  let fixture: ComponentFixture<InviteRecievedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteRecievedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteRecievedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
