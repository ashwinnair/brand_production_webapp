import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CustomerService } from '../../../customer.service';

@Component({
  selector: 'app-invite-sent',
  templateUrl: './invite-sent.component.html',
  styleUrls: ['./invite-sent.component.scss']
})
export class InviteSentComponent implements OnInit {

  connections:any = [];
  brandId:any;

  constructor(public http:HttpClient, public customer: CustomerService) {

    let brandData = JSON.parse(localStorage.getItem('userBrand'));
    this.brandId = brandData.userData.brandId;

    this.getAllConnections();
  }

  ngOnInit() {
  }

  getAllConnections(){
    for (var i=0; i < this.brandId.length; i++)
    {
      let brandID = this.brandId[i];
      if(brandID != '')
      {
        this.http.get(this.customer.AdminBaseurl + 'connection/connectionDetailsOfInitiatedByBrand/' + brandID).subscribe(
          response => {
            this.setConnectionData(response);
          }
        )
      }
    }

  }

  setConnectionData(response){

    let data = response.data;

    if(data.length > 0 )
    {
      for(var j=0; j<data.length; j++ )
      {
        let dataToInsert = data[j];
        this.connections.push(dataToInsert);
      }
    }

  }

}
