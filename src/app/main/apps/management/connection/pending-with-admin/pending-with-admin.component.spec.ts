import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingWithAdminComponent } from './pending-with-admin.component';

describe('PendingWithAdminComponent', () => {
  let component: PendingWithAdminComponent;
  let fixture: ComponentFixture<PendingWithAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingWithAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingWithAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
