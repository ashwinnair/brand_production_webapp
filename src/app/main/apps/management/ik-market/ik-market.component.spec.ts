import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IkMarketComponent } from './ik-market.component';

describe('IkMarketComponent', () => {
  let component: IkMarketComponent;
  let fixture: ComponentFixture<IkMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IkMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IkMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
