import {Component, OnInit, ViewChild} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerService } from '../../customer.service';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-ik-market',
  templateUrl: './ik-market.component.html',
  styleUrls: ['./ik-market.component.scss']
})
export class IkMarketComponent implements OnInit {

    allRetailers: any;
    companyId:any;
    allBrands:any;
    addconnection:FormGroup;
    userdata:any;
    rejectconnection:FormGroup;


    constructor(
      private http: HttpClient,
      private customer: CustomerService,
      private router:Router,
      private fb: FormBuilder,
      public ngxSmartModalService: NgxSmartModalService,
    ) {

      this.addconnection = fb.group({
        'brand_id':['']
      });
      this.rejectconnection = fb.group({
        'comment':['']
      });

    }

    ngOnInit() {

      const id = JSON.parse(localStorage.getItem('userBrand'));
      this.companyId = id.userData.companyId;
      this.userdata = id.userData._id;
      this.getIkmarketretailers();
      this.getBrands();
    }

    getBrands(){
      this.http.get(this.customer.AdminBaseurl + 'brand/brandsByCompany/'+this.companyId)
        .subscribe(
          response => {
            this.setdata(response);
          });
    }

    setdata(response){
          this.allBrands = response.data;
    }

    getIkmarketretailers(){
      this.http.get(this.customer.AdminBaseurl + 'connection/getAllIkmarketRetailers/'+ this.companyId)
        .subscribe(
          response => {
            this.setretailer(response)
          },
          response => {
            alert(response);
      });
    }

    setretailer(response){
      if(response.data.length > 0)
      this.allRetailers = response.data;

    }

    gotoretailer(data){
      localStorage.setItem('retailerprofile',JSON.stringify(data));
      this.router.navigateByUrl('/apps/profile-retailer/' + data._id)
    }

    sendreq(){
      const body = new FormData();
      body.append('connectionInitiatedBrandId',this.addconnection.get('brand_id').value);
      body.append('connectionInitiatedUserId',this.userdata);
      body.append('connectionInitiatedRetailerId',this.retailerID);
      body.append('companyId',this.companyId);
      this.http.post(this.customer.AdminBaseurl + 'connection/createConnection/',body)
      .subscribe(response => {
        this.getIkmarketretailers();
        this.ngxSmartModalService.getModal('addopsteam').close();
      })
    }

    changestatus(i,id){
      const body = new FormData();
      body.append('connectionStatus',i);
      body.append('connectionId',id);
      body.append('connectionAcceptedOrRejectedUserId',this.userdata);
      this.http.patch(this.customer.AdminBaseurl + 'connection/updateConnection',body)
      .subscribe(response => {
        this.getIkmarketretailers();
      })
    }
    connectionId:any;
    selectconnectid(id){
      this.connectionId = id;
    }

    RejectConnection(i){
      const body = new FormData();
      body.append('commnet',this.rejectconnection.get('comment').value);
      body.append('connectionStatus',i);
      body.append('connectionAcceptedOrRejectedUserId',this.userdata);
      body.append('connectionId',this.connectionId);
      this.http.patch(this.customer.AdminBaseurl + 'connection/updateConnection',body)
      .subscribe(response => {
        this.ngxSmartModalService.getModal('reject').close();
      })
    }

    retailerID:any;
    setretailerID(data){
      console.log(data);
      this.retailerID = data.data._id;
    }
}
