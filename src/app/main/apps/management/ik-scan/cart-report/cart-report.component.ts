import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerService } from '../../../customer.service';
import { EventEmiterService } from 'app/event.emmiter.service';
import { Router,ActivatedRoute } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, FormGroup, Validators,FormArray,AbstractControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExcelService } from '../../../../../services/excel.service';

@Component({
  selector: 'app-cart-report',
  templateUrl: './cart-report.component.html',
  styleUrls: ['./cart-report.component.scss']
})
export class CartReportComponent implements OnInit {
  venueId: string;
  venueUserId: string;
  Brands: any;
  selectedoption:any = '';
  columns:any = [];
  columnvis:boolean = false;
  columnviscount: any;
  btn:any;
  cartData:any = [];

  styleType:any;
  stylePercentage:any =100;
  comboType:any =100;
  comboPercentage:any =100;

  constructor(
    private http: HttpClient,
    private customer: CustomerService,
    private router: Router,
    private route: ActivatedRoute,
    public EventEmiter: EventEmiterService,
    public ngxSmartModalService: NgxSmartModalService,
    public fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private excelService:ExcelService
  ) {

  }

  ngOnInit() {
    this.venueId = this.route.snapshot.paramMap.get('venueId');
    this.venueUserId = this.route.snapshot.paramMap.get('venueUserId');
    this.configurePage();
    this.setcolumns();
  }

  configurePage(){
    this.http.get(this.customer.AdminBaseurl+'venueBooking/venueBookingDetailById/'+this.venueId).subscribe(response => {
      this.showBrands(response);
    });
  }

  showBrands(response){
    if(response.status){
      this.Brands = response.data.brand_id;
    }
  }

    setcolumns(){
    this.columns = [
  { value: 'EAN Code',  visibility : true},
  { value:'Style code', visibility : true },
  { value:'Quantity', visibility : true },
  { value:'Style Name', visibility : false },
  { value:'Style Description', visibility : false },
  { value:'IK Colour Name', visibility : true },
  { value:'Size Code', visibility : true },
  { value:'MRP', visibility : true },
  { value:'Season name', visibility : false },
  { value:'Season Code', visibility : false },
  { value:'Year', visibility : false },
  { value:'Segment', visibility : true },
  { value:'Portfolio', visibility : true },
  { value:'Department', visibility : true },
  { value:'Vertical', visibility : true },
  { value:'Category', visibility : true },
  { value:'Brick', visibility : true },
  { value:'Occasion', visibility : false },
  { value:'Sleeve', visibility : false },
  { value:'Neck', visibility : false },
  { value:'Collar', visibility : false },
  { value:'Pattern', visibility : false },
  { value:'Fabric', visibility : false },
  { value:'Fit', visibility : false },
  { value:'Material', visibility : false },
  { value:'Outer Material', visibility : false },
  { value:'Inner Material', visibility : false },
  { value:'Sole Material', visibility : false },
  { value:'Insole', visibility : false },
  { value:'leather type', visibility : false },
  { value:'Closure type', visibility : false },
  { value:'Strap type', visibility : false },
  { value:'Strap drop length unit', visibility : false },
  { value:'Strap drop length', visibility : false },
  { value:'Number of compartments', visibility : false },
  { value:'Composition', visibility : false },
  { value:'Weave', visibility : false },
  { value:'Technique', visibility : false },
  { value:'Length', visibility : false },
  { value:'Hemline', visibility : false },
  { value:'Rise', visibility : false },
  { value:'Pack Qty', visibility : false },
  { value:'Pleat', visibility : false },
  { value:'Closure type', visibility : false },
  { value:'Weather', visibility : false },
  { value:'Height map', visibility : false },
  { value:'Width', visibility : false },
  { value:'toe style', visibility : false },
  { value:'Arch', visibility : false },
  { value:'Cleats', visibility : false },
  { value:'Cushioning', visibility : false },
  { value:'heel type', visibility : false },
  { value:'Heel height unit', visibility : false },
  { value:'Heel height', visibility : false },
  { value:'platform height unit', visibility : false },
  { value:'platform height', visibility : false },
  { value:'Technology', visibility : false },
  { value:'Dimensions unit', visibility : false },
  { value:'Height (H)', visibility : false },
  { value:'Length (L)', visibility : false },
  { value:'Width (W)', visibility : false },
  { value:'Style type', visibility : false },
  { value:'Special Feature', visibility : false },
  { value:'Special Feature 1', visibility : false },
  { value:'Special Feature 2', visibility : false },
  { value:'Special Feature 3', visibility : false }
     ]
     this.setcolviscount();
    }

  togglecolumnvis(){
    if(this.columnvis){
    this.columnvis = false;}
    else
    this.columnvis = true
  }
  setcolviscount(){
    var count = this.columns.filter((obj) => obj.visibility === true).length;
    this.columnviscount = count;
  }

  generateReportatSku()
  {
    this.btn = 1;

      const body = {
        venue_id:[this.venueId],
        brand_id: this.selectedoption
      }
      this.http.post(this.customer.AdminBaseurl+'ikScanCartAnalytics/cartReportByEAN',body).subscribe(response => {
        this.handleapires(response);
      });

  }

  generateReportatoption(){
    this.btn = 2  ;
      this.columns[0].visibility = false;
    const body = {
      venue_id:[this.venueId],
      brand_id: this.selectedoption
    }
    this.http.post(this.customer.AdminBaseurl+'ikScanCartAnalytics/cartReportByComboCode',body).subscribe(response => {
      this.handleapires(response);
    });
  }

  handleapires(response){
    if(response.status){
      this.cartData = response.data;
      console.log(this.cartData[0]);
    }
  }

  generateClosingReportatStylecode(){
    this.ngxSmartModalService.getModal('closingReportByStyle').open();

  }
  closingReportStyleApi(){

    this.btn = 3;
    this.columns[0].visibility = false;

    var body = {}

    if(this.styleType == "top"){
      body = {
        venue_id:[this.venueId],
        groupBy:"style_code",
  	    sortBy:"Quantity",
  	    topPercentage:this.stylePercentage
      }
    }
    else{
      body = {
        venue_id:[this.venueId],
        groupBy:"style_code",
  	    sortBy:"Quantity",
  	    bottomPercentage:this.stylePercentage
      }
    }

    this.http.post(this.customer.AdminBaseurl+'ikScanCartAnalytics/closingReport',body).subscribe(response => {
      this.handleClosingReportStyle(response);
    });
  }
  handleClosingReportStyle(response){
    if(response.status){
      this.cartData = response.data;
      this.ngxSmartModalService.getModal('closingReportByStyle').close();
    }
  }

  generateClosingReportatCombocode(){

    this.ngxSmartModalService.getModal('closingReportByCombo').open();

  }
  closingReportComboApi(){

    this.btn = 4;
    this.columns[0].visibility = false;

    var body = {}

    if(this.comboType == "top"){
      body = {
        venue_id:[this.venueId],
        groupBy:"combocode",
  	    sortBy:"Quantity",
  	    topPercentage:this.comboPercentage
      }
    }
    else{
      body = {
        venue_id:[this.venueId],
        groupBy:"combocode",
  	    sortBy:"Quantity",
  	    bottomPercentage:this.comboPercentage
      }
    }

    this.http.post(this.customer.AdminBaseurl+'ikScanCartAnalytics/closingReport',body).subscribe(response => {
      this.handleClosingReportCombo(response);
    });
  }
  handleClosingReportCombo(response){
    if(response.status){
      this.cartData = response.data;
      this.ngxSmartModalService.getModal('closingReportByCombo').close();
    }
  }




  exportAsXLSX():void {
    var filename = "CartReport"+ Date.now();
    this.excelService.exportAsExcelFile(this.cartData, filename);
}

}
