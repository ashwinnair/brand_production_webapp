import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IkScanCartComponent } from './ik-scan-cart.component';

describe('IkScanCartComponent', () => {
  let component: IkScanCartComponent;
  let fixture: ComponentFixture<IkScanCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IkScanCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IkScanCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
