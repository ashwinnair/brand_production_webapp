import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerService } from '../../../customer.service';
import { EventEmiterService } from 'app/event.emmiter.service';
import { Router,ActivatedRoute } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, FormGroup, Validators,FormArray,AbstractControl } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-ik-scan-cart',
  templateUrl: './ik-scan-cart.component.html',
  styleUrls: ['./ik-scan-cart.component.scss']
})
export class IkScanCartComponent implements OnInit {

  distributorForm:FormGroup;

  venueId:any;
  selectedvenueUserId:any;
  selectedvenueUserType:any;
  venueCode:any;
  venueUsers:any = [];
  brands:any = { data:[] };
  distributors:any = { data:[] };
  states:any = [];
  connectionterms:FormArray;
  connectionterm: any;

  cartdetails:any = [];
  costdetails:any =[];
  modaldata:any;
  pinForVefication:any ='';

  consolidatedCartView:boolean = false;


  constructor(
    private http: HttpClient,
    private customer: CustomerService,
    private router: Router,
    private route: ActivatedRoute,
    public EventEmiter: EventEmiterService,
    public ngxSmartModalService: NgxSmartModalService,
    public fb: FormBuilder,
    private _snackBar: MatSnackBar
  ) {

  this.distributorForm = fb.group({
    'name':[''],
    'company_name':[''],
    'retailer_name':[''],
    'address':[''],
    'city':[''],
    'state':[''],
    'pincode':[''],
    'mobile':[''],
    'email':[''],
    'intrakraft_email':[''],
    'connectionterms': fb.array([]),
  });

  }

  createConnectionTerms():FormGroup{
    return this.fb.group({
      'brand_id':[''],
      'distributor_id':['']
    })
  }

  addConnectionTerms():void{
    this.connectionterm = this.distributorForm.get('connectionterms') as FormArray;
    this.connectionterm.push(this.createConnectionTerms());
  }



  ngOnInit() {
    this.venueId = this.route.snapshot.paramMap.get('venueId');
    this.getVenueUsers();
    this.getVenueData();
    this.getStates();
  }

  redirectToCartReport(){
    this.router.navigateByUrl('/apps/management/ik-scan/'+ this.venueId + '/cartreport/' + this.selectedvenueUserId);
  }

  getVenueUsers(){
    this.http.get(this.customer.AdminBaseurl + 'venueBookingUsers/allVenueBookingUsers/'+ this.venueId).subscribe(response =>{
    this.setVenueUsersData(response);
  });
  }

  setVenueUsersData(response)
  {
    if(response.status){
      this.venueUsers = response.data;
      /// to show first item cart
      this.selectedvenueUserType = response.data.account_type;
      this.showCartOfUser(this.venueUsers[0]._id);
    }

  }
  getVenueData(){
    this.http.get(this.customer.AdminBaseurl + 'venueBooking/venueBookingDetailById/'+ this.venueId).subscribe(response =>{
    this.setVenueData(response);
  });
  }
  setVenueData(response)
  {
    if(response.status){
      this.venueCode = response.data.venue_booking_id;

      this.setBrands(response);
      this.getDistributors();
    }

  }

  setBrands(response){
      this.connectionterm = this.distributorForm.get('connectionterms') as FormArray;

      this.brands = response.data.brand_id;
      for(let x of this.brands){
        this.connectionterm.push(
          this.fb.group({
            'brand_id': [x._id],
            'distributor_id':['']
          })
        );
      }
  }
  getDistributors(){
      this.http.get(this.customer.AdminBaseurl + 'venueBookingUsers/venueBookingDistributorsByVenue/'+ this.venueId).subscribe(response =>{
      this.setDistributors(response);
    });
  }
  setDistributors(response){
    if(response.status){
      this.distributors = response.data;

    }
  }

  getStates(){
    this.http.get(this.customer.AdminBaseurl + 'merchandiseMaster/allStates').subscribe(
      response => {
        this.setStates(response);
      })
  }
  setStates(response) {
    if(response.status){
      this.states = response.data;
    }
  }

  addDistributors(){
    console.log(this.distributorForm.value);
    const body = new FormData();

      body.append('venue_booking_id',this.venueId);
      body.append('venue_booking_code',this.venueCode);
      body.append('name',this.distributorForm.get('name').value);
      body.append('account_type','Distributor');
      body.append('company_name',this.distributorForm.get('company_name').value);
      body.append('address',this.distributorForm.get('address').value);
      body.append('city',this.distributorForm.get('city').value);
      body.append('state',this.distributorForm.get('state').value);
      body.append('pincode',this.distributorForm.get('pincode').value);
      body.append('mobile',this.distributorForm.get('mobile').value);
      body.append('email',this.distributorForm.get('email').value);
      body.append('connection_terms',JSON.stringify(this.distributorForm.get('connectionterms').value));
      body.append('intrakraft_email_forDistributor',this.distributorForm.get('intrakraft_email').value);

    this.http.post(this.customer.AdminBaseurl + 'venueBookingUsers/createVenueBookingUsers',body).subscribe(response => {
      this.handleDistributorAddResponse(response);
    })
  }

  handleDistributorAddResponse(response){
    this.ngxSmartModalService.getModal('addDistributor').close();
    this.getVenueUsers();
  }

  addRetailers(){
    console.log(this.distributorForm.value);
    const body = new FormData();
      body.append('venue_booking_id',this.venueId);
      body.append('venue_booking_code',this.venueCode);
      body.append('name',this.distributorForm.get('name').value);
      body.append('account_type','Retailer');
      body.append('company_name',this.distributorForm.get('company_name').value);
      body.append('retail_chain_name',this.distributorForm.get('retailer_name').value);
      body.append('address',this.distributorForm.get('address').value);
      body.append('city',this.distributorForm.get('city').value);
      body.append('state',this.distributorForm.get('state').value);
      body.append('pincode',this.distributorForm.get('pincode').value);
      body.append('mobile',this.distributorForm.get('mobile').value);
      body.append('email',this.distributorForm.get('email').value);
      body.append('connection_terms',JSON.stringify(this.distributorForm.get('connectionterms').value));

    this.http.post(this.customer.AdminBaseurl + 'venueBookingUsers/createVenueBookingUsers',body).subscribe(response => {
      this.handleRetailerAddResponse(response);
    })
  }
  handleRetailerAddResponse(response){
    this.ngxSmartModalService.getModal('addRetailer').close();
    this.getVenueUsers();
  }

  get formValue() {
    return this.distributorForm.get('connectionterms') as FormArray;
  }

  showCartOfUser(userId){
    this.selectedvenueUserId = userId;
    this.consolidatedCartView = false;
    this.http.get(this.customer.AdminBaseurl + 'ikScanCart/listingofcarts/' + userId).subscribe(
      response => {
        this.setCartData(response);
      })
  }
  setCartData(response){
    if(response.status){
      this.cartdetails = response.data;
      this.setquantity(response.data);
    }
  }

  setquantity(response){
   this.costdetails = response;
  }

  sizedata(data){
    var order = ["FS","XXXS","XXS",'XS','S','M','L','XL','XXL',"XXXL","XXXXL","XSS","SM","ML","LXL","NB","00","02","04","06","08","10","12","14","16","18","20","22","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","42","44","46","48","50","52","54","56","58"];
    var array = [];
    for(let x of order){
      for (let y of data){
        if(x == y.catalogue.Size_Code){
            array.push(y);
        }
      }
    }
    return array;
  }

  returnqty(qty){
    return qty.totalquantity;
  }
  returnamt(amt){
    return amt.totalAmount;
  }

  getinventory(size,color,stylecode,i,ix,iy,iz,products){
    color = color.replace('#','');
    var productid:string = 'b'+i+ix+iy+iz;
    var  value = parseFloat((document.getElementById(productid) as HTMLInputElement).value);
    this.http.get(this.customer.AdminBaseurl + 'ikScanCatalogue/catalogeDetailbySCndCC/'+color+'/'+stylecode+'/'+size).subscribe(
      response => {
    // this.checkdata(value,response.data,productid);
     this.cartUpdation(value, response);
    })

  }

  cartUpdation(value,response){
    if(response.status){
      const body = {
            "venueId": this.venueId,
            "venueUserId": this.selectedvenueUserId,
            "ean_no": response.data[0].EAN_code,
            "input_qty": value
      }

      this.http.put(this.customer.AdminBaseurl + 'ikScanCart/insertqty',body).subscribe(response => {
        this.getcostinfo();

      })
    }

  }

  getcostinfo(){
    this.http.get(this.customer.AdminBaseurl + 'ikScanCart/listingofcarts/' + this.selectedvenueUserId).subscribe(response=>{
      this.setCostInfo(response);
    });
  }
  setCostInfo(response){
    this.setquantity(response.data);
  }

  updateSetsCount(brandId,color,stylecode,event){

    const body = {
          "venueId": this.venueId,
          "venueUserId": this.selectedvenueUserId,
          "brandId": brandId,
          "styleCode": stylecode,
          "colorCode": color,
          "setQuantity": event.target.value
    }

    this.http.put(this.customer.AdminBaseurl + 'ikScanCart/updateSetCount',body).subscribe(response => {
      this.getcostinfo();

    })

  }

  deleteParticularItem(stylecode,colorcode,brandId){

    const body = {
      "stylecode": stylecode,
      "colorcode":colorcode,
      "venueId": this.venueId,
      "venueUserId": this.selectedvenueUserId,
      "brandId" : brandId

    }

     this.http.post(this.customer.AdminBaseurl + 'ikScanCart/deleteIteminCartBySingle', body).subscribe(response=>{
      this.showCartOfUser(this.selectedvenueUserId);
    })
  }

  unfreezeBrandCart(brandId){

    const body = {
      "venueId": this.venueId,
      "venueUserId": this.selectedvenueUserId,
      "brandId" : brandId

    }

     this.http.put(this.customer.AdminBaseurl + 'ikScanCart/unfreezeCartByBrand', body).subscribe(response=>{
      this.showCartOfUser(this.selectedvenueUserId);
    })
  }

  verifyPinAction(){

    const body = {
      "venueId": this.venueId,
      "venueUserId": this.selectedvenueUserId,
      "brandId" : this.modaldata,
      "pin" : this.pinForVefication

    }

     this.http.put(this.customer.AdminBaseurl + 'ikScanCart/verifyPinByBrand', body).subscribe(response=>{
      this.setEditOrderData(response);
    })
  }
  setEditOrderData(response){
    if(response.status){
      this.ngxSmartModalService.getModal('pinVerification').close();
      this.showCartOfUser(this.selectedvenueUserId);
    }
    else{
      this._snackBar.open('Invalid PIN','', {
     duration: 2000,
   });
    }

  }

  getConsildatedCartData(){
    this.consolidatedCartView = true;
    this.http.get(this.customer.AdminBaseurl + 'ikScanCart/consolidatedListingOfCartsForDistributor/'+this.selectedvenueUserId).subscribe(
      response => {
        this.setCartData(response);
    })

  }



//   checkdata(value,response,productid){
//     console.log(response);
//     if(response !='')
// {
//     if(response != ''){
//       if(value > response[0].total_quantity){
//         this.changeclass(productid);
//       }
//       else if(value <= response[0].total_quantity){
//         this.changeclassback(productid);
//         this.api.updatecart(response[0].EAN_code,value,this.id,this.selectedStoreId).subscribe(response => {
//           this.getcostinfo();
//         //this.getcartinfo();
//
//         })
//       }
//     }}
//   }

  // changeclass(productid){
  //  var divToChange = document.getElementById(productid);
  //  divToChange.className = "number-input error";
  // }
  // changeclassback(productid){
  //  var divToChange = document.getElementById(productid);
  //  divToChange.className = "number-input";
  // }




}
