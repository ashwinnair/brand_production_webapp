import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IkScanComponent } from './ik-scan.component';

describe('IkScanComponent', () => {
  let component: IkScanComponent;
  let fixture: ComponentFixture<IkScanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IkScanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IkScanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
