import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerService } from '../../customer.service';
import { EventEmiterService } from 'app/event.emmiter.service';
import { Router,ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-ik-scan',
  templateUrl: './ik-scan.component.html',
  styleUrls: ['./ik-scan.component.scss']
})
export class IkScanComponent implements OnInit {

  allVenues: any;

  constructor(
    private http: HttpClient,
    private customer: CustomerService,
    private router: Router,
    private route: ActivatedRoute,
    public EventEmiter: EventEmiterService
  ) {

  }

  ngOnInit() {
    const data = JSON.parse(localStorage.getItem('BrandInfo'));

    this.http.get(this.customer.AdminBaseurl + 'venueBooking/venueBookingsByCompany/'+data.companyId)
      .subscribe(
        response => {
          this.setdata(response);
        });
  }

  setdata(response){
    this.allVenues = response.data;
  }
  gotocart(venue){
    console.log("Cart clickd");
    let url = '/apps/management/ik-scan/' + venue._id;
    this.router.navigateByUrl(url);
  }


}
