import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule, FuseConfirmDialogModule } from '@fuse/components';


import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatStepperModule,
    MatListModule,
    MatTabsModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatRadioModule,
    MatChipsModule,
    MatSnackBarModule,
    MatBadgeModule,
    MatTooltipModule,
    MatExpansionModule,
  } from '@angular/material';


import { PartnerManagementComponent } from './partner-management/partner-management.component';
import { AddDistributorComponent } from './add-distributor/add-distributor.component';
import { AddRetailerComponent } from './add-retailer/add-retailer.component';
import { AccountComponent } from './account/account.component';
import { ProfileComponent } from './account/profile/profile.component';
import { WarehouseComponent } from './account/warehouse/warehouse.component';
import { TeamComponent } from './account/team/team.component';
import { BankingComponent } from './account/banking/banking.component';
import { AddTeamComponent } from './account/add-team/add-team.component';
import { AddWarehouseComponent } from './account/add-warehouse/add-warehouse.component';
import { IkMarketComponent } from './ik-market/ik-market.component';
import { DetailsComponent } from './account/details/details.component';
import { ConnectionComponent } from './connection/connection.component';
import { ConnectedComponent } from './connection/connected/connected.component';
import { ConnectionDetailsComponent } from './connection/connection-details/connection-details.component';
import { InviteRecievedComponent } from './connection/invite-recieved/invite-recieved.component';
import { InviteSentComponent } from './connection/invite-sent/invite-sent.component';
import { PendingWithAdminComponent } from './connection/pending-with-admin/pending-with-admin.component';
import { IkScanComponent } from './ik-scan/ik-scan.component';
import { IkScanCartComponent } from './ik-scan/ik-scan-cart/ik-scan-cart.component';
import { CartReportComponent } from './ik-scan/cart-report/cart-report.component';

const routes: Routes = [
    {
        path: 'connection',
        component: ConnectionComponent
    },
    {
        path: 'partner-management',
        component: PartnerManagementComponent
    },
    {
        path: 'account',
        component: AccountComponent
    },
    {
        path: 'ik-market',
        component: IkMarketComponent
    },
    {
        path: 'ik-scan',
        component: IkScanComponent
    },
    {
        path: 'ik-scan/:venueId',
        component: IkScanCartComponent
    },
    {
        path: 'ik-scan/:venueId/cartreport/:venueUserId',
        component: CartReportComponent
    },
    {
        path: 'connection/connectionDetails/:connectionId',
        component: ConnectionDetailsComponent
    },
];

@NgModule({
    declarations: [
      PartnerManagementComponent,
      AddDistributorComponent,
      AddRetailerComponent,
      AccountComponent,
      ProfileComponent,
      WarehouseComponent,
      TeamComponent,
      BankingComponent,
      AddTeamComponent,
      AddWarehouseComponent,
      IkMarketComponent,
      DetailsComponent,
      ConnectionComponent,
      ConnectedComponent,
      ConnectionDetailsComponent,
      InviteRecievedComponent,
      InviteSentComponent,
      PendingWithAdminComponent,
      IkScanComponent,
      IkScanCartComponent,
      CartReportComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        NgxSmartModalModule.forRoot(),
        TranslateModule,
        MatSlideToggleModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatListModule,
        MatTabsModule,
        MatSortModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatRadioModule,
        MatChipsModule,
        MatBadgeModule,
        MatCardModule,
        MatTooltipModule,
        FuseSharedModule,
        MatSnackBarModule,
        FuseSidebarModule,
        FuseConfirmDialogModule,
        MatExpansionModule,
    ],
    entryComponents: [
        AddTeamComponent,
        AddWarehouseComponent
    ]
})

export class ManagementModule {
}
