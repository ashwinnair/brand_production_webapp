import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-partner-management',
  templateUrl: './partner-management.component.html',
  styleUrls: ['./partner-management.component.scss']
})
export class PartnerManagementComponent implements OnInit {

  status:number = 1;
  activetab1:any = '';
  activetab2:any = 'active';
  constructor() { }

  ngOnInit() {
  }
  changetoConnected(){
    this.status = 0;
    this.activetab1 = true;
    this.activetab2 = false;


  }
  changetoPWA(){
    this.status = 1;
    this.activetab1 = false;
    this.activetab2 = true;
  }

}
