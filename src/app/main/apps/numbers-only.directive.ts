import {Injectable} from '@angular/core';

const TOKEN = 'TOKEN';
const UserData = 'UserData';


@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  userRegistrationData : any = {};

  Baseurl = 'http://192.168.1.109:3000/api/';
  // Baseurl = 'http://34.200.157.134:3000/api/';

  setToken(token: string): void {
    localStorage.setItem(TOKEN, token);
  }

  setUserData(data) {
    localStorage.setItem(UserData, data);
  }

  isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }

  userDetails(data){
    // console.log(data);
    this.userRegistrationData = data;
  }

}
