import { Component, ViewEncapsulation } from '@angular/core';
import { ProfileService } from './profile.service';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';

@Component({
    selector     : 'profile',
    templateUrl  : './profile.component.html',
    styleUrls    : ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileComponent
{
    imageUrls: any = [
        { url: '/assets/images/banner/raymond01.jpg'},
        { url: '/assets/images/banner/raymond01.jpg'},
        { url: '/assets/images/banner/raymond01.jpg'}
    ];
    height: string = '400px';
    minHeight: string;
    arrowSize: string = '30px';
    showArrows: boolean = true;
    disableSwiping: boolean = false;
    autoPlay: boolean = true;
    autoPlayInterval: number = 3333;
    stopAutoPlayOnSlide: boolean = true;
    debug: boolean = false;
    backgroundSize: string = 'cover';
    backgroundPosition: string = 'center center';
    backgroundRepeat: string = 'no-repeat';
    showDots: boolean = true;
    dotColor: string = '#FFF';
    showCaptions: boolean = true;
    captionColor: string = '#FFF';
    captionBackground: string = 'rgba(0, 0, 0, .35)';
    lazyLoad: boolean = false;
    hideOnNoSlides: boolean = false;
    width: string = '100%';
    fullscreen: boolean = false;
    data:any;
    /**
     * Constructor
     */
    constructor(public api:ProfileService, public router:Router)
    {
      this.data = JSON.parse(localStorage.getItem('retailerprofile'));
      this.checkbannerdata(this.data);

    }
    checkbannerdata(data){
      if(this.data.banner_images.length == 0){
        this.data.banner_images = ['https://utmsi.utexas.edu/components/com_easyblog/themes/wireframe/images/placeholder-image.png'];
      }
    }


}
