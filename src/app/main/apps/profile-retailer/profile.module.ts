import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatDividerModule, MatIconModule, MatTabsModule, MatFormFieldModule, MatInputModule, MatSelectModule, } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import {SlideshowModule} from 'ng-simple-slideshow';
import { QuillModule } from 'ngx-quill';
import { ProfileService } from './profile.service';
import { ProfileComponent } from './profile.component';
import { ProfileTimelineComponent } from './tabs/timeline/timeline.component';
import { ProfileAboutComponent } from './tabs/about/about.component';
import { ProfileCompanyComponent } from './tabs/company/company.component';
import { ProfileTeamComponent } from './tabs/team/team.component';
import { ProfileWarehouseComponent } from './tabs/warehouse/warehouse.component';
 import { ProfileStoreComponent } from './tabs/store/store.component';
import { ProfileBankinfoComponent } from './tabs/bank-info/bank-info.component';
import { ProfilePhotosVideosComponent } from './tabs/photos-videos/photos-videos.component';

const routes = [
    {
        path     : '**',
        component: ProfileComponent,
        resolve  : {
            profile: ProfileService
        }
    }
];

@NgModule({
    declarations: [
        ProfileComponent,
        ProfileTimelineComponent,
        ProfileAboutComponent,
        ProfileTeamComponent,
        ProfileCompanyComponent,
        ProfileWarehouseComponent,
      ProfileStoreComponent,
        ProfileBankinfoComponent,
        ProfilePhotosVideosComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        QuillModule.forRoot(),
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        SlideshowModule,
        SlideshowModule,

        FuseSharedModule
    ],
    providers   : [
        ProfileService
    ]
})
export class ProfileModule
{
}
