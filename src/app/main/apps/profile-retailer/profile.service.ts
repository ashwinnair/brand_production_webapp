import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { CustomerService } from '../customer.service';


@Injectable()
export class ProfileService implements Resolve<any>
{


    constructor(
        private http: HttpClient,
        private customer: CustomerService
    )
    {

    }


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

    }

    getAllstores(id):Observable <any>{
      return this.http.get(this.customer.AdminBaseurl + 'store/storesByRetailer/'+ id);
    }
    getAllteams(id):Observable <any>{
      return this.http.get(this.customer.AdminBaseurl + 'retailer/auth/teamsByRetailer/'+ id);
    }
    // getAllwarehouses(id):Observable <any>{
    //   return this.http.get(this.customer.AdminBaseurl + 'retailerWarehouse/warehousesByRetailer/'+ id);
    // }
    getAllwarehouses(id):Observable <any>{
      return this.http.get(this.customer.AdminBaseurl + 'retailerDeliveryLocation/deliveryLocationsByRetailer/'+ id);
    }
    getAllbanks(id):Observable <any>{
      return this.http.get(this.customer.AdminBaseurl + 'retailerBank/banksByRetailer/'+ id);
    }



}
