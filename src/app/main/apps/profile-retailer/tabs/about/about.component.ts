import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';

@Component({
    selector     : 'profile-about',
    templateUrl  : './about.component.html',
    styleUrls    : ['./about.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileAboutComponent implements OnInit
{
    data: any;

    viewsocial:any = true;

    constructor(
        private _profileService: ProfileService,
        private _formBuilder: FormBuilder,
    )
    {
            this.data = JSON.parse(localStorage.getItem('retailerprofile'));
            console.log(this.data);
    }

    ngOnInit(): void {}



}
