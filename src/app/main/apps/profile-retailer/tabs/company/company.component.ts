import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';
import { TreeMapModule } from '@swimlane/ngx-charts';

@Component({
    selector     : 'profile-company',
    templateUrl  : './company.component.html',
    styleUrls    : ['./company.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileCompanyComponent implements OnInit
{
    company: any;
    id: any;

    companyDetails: FormGroup;
    corporateOfficeDetails: FormGroup;



    constructor(
        private _profileService: ProfileService,
        private _formBuilder: FormBuilder,
    )
    {

    }

    ngOnInit(): void
    {}

}
