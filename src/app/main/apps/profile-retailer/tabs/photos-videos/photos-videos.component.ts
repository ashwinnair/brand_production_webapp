import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { Lightbox } from 'ngx-lightbox';
import { ProfileService } from '../../profile.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector     : 'profile-photos-videos',
    templateUrl  : './photos-videos.component.html',
    styleUrls    : ['./photos-videos.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfilePhotosVideosComponent implements OnInit
{

    data:any;
    lightboxalbum:any= [];

    constructor( private _lightbox: Lightbox) {
      this.data = JSON.parse(localStorage.getItem('retailerprofile'));
      for(let x of this.data.gallery){
        const album = {
         src: x,
      };
            this.lightboxalbum.push(album);
      }
    }

    ngOnInit() {
    }


    open(index) {
      console.log(index);
  this._lightbox.open(this.lightboxalbum, index,{resizeDuration:0.8,fadeDuration:1,enableTransition:false});
}

    close() {
      this._lightbox.close();
    }

}
