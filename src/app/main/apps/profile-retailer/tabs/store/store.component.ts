import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';

@Component({
    selector     : 'profile-store',
    templateUrl  : './store.component.html',
    styleUrls    : ['./store.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileStoreComponent implements OnInit
{
  data:any;
  stores:any;

  constructor(public api:ProfileService) {
        this.data = JSON.parse(localStorage.getItem('retailerprofile'));
  }

  ngOnInit() {
    this.api.getAllstores(this.data._id).subscribe(response => {
      console.log(response);
      this.stores = response.data;
    })
  }
}
