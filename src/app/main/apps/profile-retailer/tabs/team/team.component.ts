import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';
import { TreeMapModule } from '@swimlane/ngx-charts';

@Component({
    selector     : 'profile-team',
    templateUrl  : './team.component.html',
    styleUrls    : ['./team.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileTeamComponent implements OnInit
{
  data:any;
  teams:any;

  constructor(public api:ProfileService) {
        this.data = JSON.parse(localStorage.getItem('retailerprofile'));
  }

  ngOnInit() {
    this.api.getAllteams(this.data._id).subscribe(response => {
      console.log(response);
      this.teams = response.data;
    })
  }

}
