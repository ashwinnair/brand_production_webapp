import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';



@Component({
    selector     : 'profile-warehouse',
    templateUrl  : './warehouse.component.html',
    styleUrls    : ['./warehouse.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileWarehouseComponent implements OnInit
{
  data:any;
  warehouses:any;

  constructor(public api:ProfileService) {
        this.data = JSON.parse(localStorage.getItem('retailerprofile'));
  }

  ngOnInit() {
    this.api.getAllwarehouses(this.data._id).subscribe(response => {
      console.log(response);
      this.warehouses = response.data;
    })
  }
}
