import { Component, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from './profile.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable, interval } from 'rxjs';

@Component({
    selector     : 'profile',
    templateUrl  : './profile.component.html',
    styleUrls    : ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileComponent
{


    data:any;
    about:any;
    imageUrls: any = [
        { url: '/assets/images/banner/raymond01.jpg'},
        { url: '/assets/images/banner/raymond01.jpg'},
        { url: '/assets/images/banner/raymond01.jpg'}
    ];
    height: string = '400px';
    minHeight: string;
    arrowSize: string = '30px';
    showArrows: boolean = true;
    disableSwiping: boolean = false;
    autoPlay: boolean = true;
    autoPlayInterval: number = 3333;
    stopAutoPlayOnSlide: boolean = true;
    debug: boolean = false;
    backgroundSize: string = 'cover';
    backgroundPosition: string = 'center center';
    backgroundRepeat: string = 'no-repeat';
    showDots: boolean = true;
    dotColor: string = '#FFF';
    showCaptions: boolean = true;
    captionColor: string = '#FFF';
    captionBackground: string = 'rgba(0, 0, 0, .35)';
    lazyLoad: boolean = false;
    hideOnNoSlides: boolean = false;
    width: string = '100%';
    fullscreen: boolean = false;
    id:any;

    // Edit Variables

    Profile_Name:boolean = true;
    Profile_Name_Value:string;
    Profile_Name_form: FormGroup;
    Profile_Picture:FormGroup;
    banner_pics:FormGroup;

    /**
     * Constructor
     */
    constructor(private fb: FormBuilder,private profileService: ProfileService,
        private router: Router,
        private route: ActivatedRoute)
    {
      this.Profile_Name_form = fb.group({
        Profile_Name:['']
      });

      this.Profile_Picture = fb.group({
        Profile_pic:['']
      });
      this.banner_pics = fb.group({
        bannerimg:['']
      });


      this.about = this.profileService.about;
      console.log(this.about);
      this.id = this.route.snapshot.paramMap.get('id');
      console.log(this.id);
      this.getdata();


    }


        getdata(){
          this.profileService.getprofile(this.id).subscribe(response => {
            if(response.status){
              this.setdata(response)
            }

          });


        }

        setdata(response){
          this.data = response.data;
          console.log(response);
          if(this.data.banner_images != null){
            console.log("");
          }
          else{
            console.log("Emptystring");
            this.data.banner_images = ['https://static.wixstatic.com/media/0d2503_4c6fe17d563e44fe9be49d8730ff4e9a~mv2.gif'];
          }
        }



    editname(){
      this.Profile_Name = false;
    }

    okeditname(){
      this.Profile_Name = true;
      this.Profile_Name_Value = this.Profile_Name_form.get('Profile_Name').value;
      const body = new FormData();
      body.append('brand_name',this.Profile_Name_Value);
      this.profileService.updateprofile(this.id,body).subscribe(response=>{
      });
    }

    profilepicture(event){
      console.log(event);
      if(event.target.files.length > 0) {
        let file = event.target.files[0];
        console.log(file);
        this.Profile_Picture.get('Profile_pic').setValue(file);
      }
      const body = new FormData();
      body.append('logo',this.Profile_Picture.get('Profile_pic').value);
      this.profileService.updateprofile(this.id,body).subscribe(response => {
          this.getdata();
      });
      // body.append('profileImage',this.Profile_Picture.get('Profile_pic').value);
      // this.profileService.updatepic(this.id,body).subscribe(response => {
      //     this.getdata();
      // });


    }
    uploadbannerapi(event){
      const body = new FormData();
      var file1 = [];
      for(let i = 0 ; i< event.target.files.length;i++){
        body.append('banner_images'+i, event.target.files[i]);
      }
      this.data.banner_images = ["https://media1.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif"];
      this.profileService.uploadbanner(this.id,body).subscribe(response=>{

          var showElement = true;
            setTimeout(() => {
            this.getdata()
            var showElement = false;
          }, 5000);
    })

}

}
