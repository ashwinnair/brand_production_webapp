import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { CustomerService } from '../customer.service';


@Injectable()
export class ProfileService
{
    timeline: any;
    about: any;
    team: any;
    company: any;
    warehouse: any;
    store: any;
    bankinfo: any;
    photosVideos: any;



    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private customer: CustomerService
    )
    {

    }




    storeData(data, id): Observable<any> {
        return this._httpClient.patch<any>(this.customer.Baseurl + 'retailer/registration/storedata/' + id, {
            store_details: [
                {
                    store_name: data.storeName,
                    email: data.email,
                    mobile: data.mobileNumber,
                    landline: data.landlineNumber,
                    city: data.city,
                    state: data.state,
                    address: data.address,
                    pincode: data.pincode,
                    GST: data.GSTIN,
                }
            ]
            });
    }

    warehouseDeliveryData(data, id): Observable<any> {
        return this._httpClient.patch<any>(this.customer.Baseurl + 'retailer/registration/warehouse/' + id, {
            Ware_house_delivery: [ data ]
        });
    }

    authorisedSignatoryDetailsData(data, id): Observable<any> {
        return this._httpClient.patch<any>(this.customer.Baseurl + 'retailer/registration/authsigndata/' + id, {
            authorised_signatory_details: data
        });
    }

    operationsTeamDetailsData(data, id): Observable<any> {
        return this._httpClient.patch<any>(this.customer.Baseurl + 'retailer/registration/oprtndata/' + id, {
            operations_team_details: [data]
        });
    }

    companyDetailsData(data, id): Observable<any> {
        return this._httpClient.patch<any>(this.customer.Baseurl + 'retailer/registration/companydata/' + id, {
            company_details: data
        });
    }

    corporateOfficeDetailsData(data, id): Observable<any> {
        return this._httpClient.patch<any>(this.customer.Baseurl + 'retailer/registration/corporatedata/' + id, {
            Corporate_office: data
        });
    }


    addopsteam(data,id): Observable<any> {
      const body = new FormData();
      body.append('firstname',data.firstname);
      body.append('lastname',data.lastname);
      body.append('designation',data.designation);
      body.append('email_id',data.email);
      body.append('mobile_number',data.mobile);
      body.append('landline_number',data.landline);

      return this._httpClient.put(this.customer.Baseurl + 'manage/account/RetailerAddTeam/' + id,body);

      }
      addstore(data,id): Observable<any> {
        const body = new FormData();
        body.append('store_name',data.store_name);
        body.append('email',data.email);
        body.append('mobile',data.mobile);
        body.append('landline',data.landline);
        body.append('address',data.address);
        body.append('city',data.city);
        body.append('state',data.state);
        body.append('pincode',data.pincode);
        body.append('GST',data.GST);
        return this._httpClient.put(this.customer.Baseurl + 'manage/account/RetailerStoreAdd/' + id,body);

        }

        addwarehouses(data,id): Observable<any> {
          const body = new FormData();
          body.append('warehouse_name',data.warehouse_name);
          body.append('mobile',data.mobile);
          body.append('landline',data.landline);
          body.append('Email',data.Email);
          body.append('address',data.address);
          body.append('city',data.city);
          body.append('state',data.state);
          body.append('pincode',data.pincode);
          body.append('GST',data.GST);
          return this._httpClient.put(this.customer.Baseurl + 'manage/account/RetailerAddWarehouse/' + id,body);

          }

      getauthteam(id):Observable <any>{

        return this._httpClient.get(this.customer.AdminBaseurl + 'team/auth/teamsOfBrand/'+id);
      }

      getstores(id):Observable <any>{
        return this._httpClient.get(this.customer.Baseurl + 'manage/account/RetailerStoreAdd/'+id );
      }

    getPhotosVideos(): Observable<any[]>
    {
      var response;
      return response
    }

    deleteops(uid,tid):Observable<any>{
      return this._httpClient.delete(this.customer.Baseurl + 'manage/account/RetailerDelOpTeam/'+ uid +'/' +tid);
    }


    getstore(id):Observable<any>{
      return this._httpClient.get(this.customer.Baseurl+ 'manage/account/getRetailerStoreDetails/' + id);
    }

    getwarehouse(id):Observable<any>{
            return this._httpClient.get(this.customer.AdminBaseurl+ 'warehouse/warehousesOfBrand/' + id);

    }

    getcompanyWarehouse(id):Observable<any>{
            return this._httpClient.get(this.customer.AdminBaseurl+ 'warehouse/warehousesByCompany/' + id);
    }

    updateprofile(id,body):Observable<any>{
      return this._httpClient.patch(this.customer.AdminBaseurl + 'brand/updateBrand/'+ id,body);
    }
    getprofile(id):Observable<any>{
      return this._httpClient.get(this.customer.AdminBaseurl + 'brand/brandDetailsById/'+ id);
    }

    updatepic(id,body):Observable<any>{
      console.log(body);
      return this._httpClient.put(this.customer.Baseurl + 'upload/profile/profileImage/'+ id, body);
    }

    getBankinfo(id):Observable<any>{

      return this._httpClient.get(this.customer.AdminBaseurl + 'bank/banksOfBrand/' + id);
    }
    getcompanyBankinfo(id):Observable<any>{

      return this._httpClient.get(this.customer.AdminBaseurl + 'bank/banksByCompany/' + id);
    }

    uploadbanner(id,body):Observable<any>{
      return this._httpClient.patch(this.customer.AdminBaseurl + 'brand/updateImages/'+id,body);
    }

    getcompanyTeam(id):Observable<any>{
      return this._httpClient.get(this.customer.AdminBaseurl + 'team/auth/teamsByCompany/'+id);
    }

    addbanks(brandID,teamid):Observable <any>{
      const body = new FormData();
      body.append('brandId',brandID);
      return this._httpClient.patch(this.customer.AdminBaseurl + 'bank/addBrandsToBank/'+teamid,body);
    }

    removeBank(brandID,bankid):Observable <any>{
      const body = new FormData();
      body.append('brandId',brandID);
      return this._httpClient.patch(this.customer.AdminBaseurl + 'bank/removeBrandsFromBank/'+bankid,body);
    }

    uploadgal(id,body):Observable<any>{
       return this._httpClient.patch(this.customer.AdminBaseurl + 'brand/updateImages/'+id,body);
    //  return this._httpClient.post(this.customer.Baseurl + 'catalogue/uploadImages',body);
    }

    // deleteimage(index,id):Observable<any>{
    //
    //   const body = new FormData();
    //   body.append('retailerID',id);
    //   body.append('index',index);
    //   return this._httpClient.post(this.customer.Baseurl + 'upload/profile/deletegallery',body);
    // }

    deleteimage(index,id):Observable<any>{

      const body = new FormData();
      body.append('brandID',id);
      body.append('index',index);
      return this._httpClient.post(this.customer.AdminBaseurl + 'brand/deletegallery',body);
    //  return this._httpClient.post(this.customer.Baseurl + 'upload/profile/deletegallery',body);
    }

    addTeamtoBrand(brandID,teamid){

      const body = new FormData();

      body.append('brandId',brandID);
      return this._httpClient.patch(this.customer.AdminBaseurl + 'team/auth/addBrandsToTeam/'+teamid,body);

    }

    removeTeamFromBrand(brandID,teamid){

      const body = new FormData();

      body.append('brandId',brandID);
      return this._httpClient.patch(this.customer.AdminBaseurl + 'team/auth/removeBrandsFromTeam/'+teamid,body);

    }
    addwarehousetobrand(brandID,warehouseid){
      const body = new FormData();
      body.append('brandId',brandID);
      return this._httpClient.patch(this.customer.AdminBaseurl + 'warehouse/addBrandsToWarehouse/'+warehouseid,body);
    }

    removeWarehouseFromBrand(brandID,warehouseid){
      const body = new FormData();
      body.append('brandId',brandID);
      return this._httpClient.patch(this.customer.AdminBaseurl + 'warehouse/removeBrandsFromWarehouse/'+warehouseid,body);
    }

  getmasterstate(){
    this._httpClient.get(this.customer.Baseurl + 'merchandiseMaster/allStates');
  }
  getmastersegment(){
  return  this._httpClient.get(this.customer.AdminBaseurl + 'merchandiseMaster/allSegments');
  }
  getmastervertical(){
  return  this._httpClient.get(this.customer.Baseurl + 'merchandiseMaster/allVerticals');
  }
  getmastercategory(){
  return this._httpClient.get(this.customer.AdminBaseurl + 'merchandiseMaster/allCategories');
  }
  getmasterdepartment(){
  return  this._httpClient.get(this.customer.AdminBaseurl + 'merchandiseMaster/allDepartments');
  }
  getmasterportfolio(){
    return this._httpClient.get(this.customer.Baseurl + 'merchandiseMaster/allPortfolios');
  }
  getmasterbricks(){
    return this._httpClient.get(this.customer.Baseurl + 'merchandiseMaster/allBricks');
  }


}
