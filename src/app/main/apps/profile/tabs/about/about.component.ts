import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector     : 'profile-about',
    templateUrl  : './about.component.html',
    styleUrls    : ['./about.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileAboutComponent implements OnInit, OnDestroy
{
    about: any;
    id: any;
    form: FormGroup;
    data:any;
    datevalue:any;
   showgeneralInfoDoneicon = false;
   showgeneralInfoEditicon = true;

   showTopRetailerDoneicon = false;
   showTopRetailerEditicon = true;

   showTopSegmentDoneicon = false;
   showTopSegmentEditicon = true;

   showTopCategoriesDoneicon = false;
   showTopCategoriesEditicon = true;

   showTopDepartmentDoneicon = false;
   showTopDepartmentEditicon = true;

   showMRPDoneicon = false;
   showMRPEditicon = true;

   showSocialLinkDoneicon = false;
   showSocialLinkEditicon = true;

    generalInfo: FormGroup;
    topBrandsDetails: FormGroup;
    topSegmentDetails: FormGroup;
    TopCategoryDetails: FormGroup;
    topDepartmentDetails: FormGroup;
    MRPRangeDetails: FormGroup;
    socialLinkDetails: FormGroup;
    //  New Code

    viewops:boolean = true;
    viewabt:boolean = true;
    viewhq:boolean = true;
    viewtopb:boolean = true;
    viewtops:boolean = true;
    viewtopc:boolean = true;
    viewtopd:boolean = true;
    viewsocial:boolean = true;
    viewpivotal:boolean = true;





    constructor(
        private _profileService: ProfileService,
        private _formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    )
    {
        this.form = _formBuilder.group({
          'date_of_incorp':[''],
          'no_of_stores':[''],
          'size_of_store':[''],
          'company_brief':[''],
          'address':[''],
          'state': [''],
          'city':[''],
          'pincode':[''],
          'Brand1':[''],
          'Brand2':[''],
          'Brand3':[''],
          'segment1':[''],
          'segment2':[''],
          'segment3':[''],
          'categories1':[''],
          'categories2':[''],
          'categories3':[''],
          'department1':[''],
          'department2':[''],
          'department3':[''],
          'mrp_low':[''],
          'mrp_high':[''],
          'facebook':[''],
          'google_plus':[''],
          'youtube':[''],
          'linkedin':[''],
          'twitter':[''],
          'instagram':[''],
          'web':['']

        })

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {


            this.id = this.route.snapshot.paramMap.get('id');
            console.log(this.id);
            this.getdata();

    }


  getdata(){
        this._profileService.getprofile(this.id).subscribe(response => {
          if(response.status){
            this.setdata(response)
          }

        });


      }

      setdata(response){
        this.data = response.data;
        console.log("Response",response);
        this.form.patchValue({
          'company_brief':this.data.about_brand
        });

      }




    ngOnDestroy(): void
    {

    }

// New Code

  editops(){
    this.viewops = false;
  }

  okops(){
     this.viewops = true;
     const body = new FormData();

    body.append('established_on',this.form.get('date_of_incorp').value);
    body.append('point_of_sales',this.form.get('no_of_stores').value);

     this._profileService.updateprofile(this.id,body).subscribe(response=>{
        this.savehq();
        this.getdata();
     });
  }
  saveops(){
     this.viewops = true;

  }


  editabt(){
    this.viewabt = false;
  }

  okabt(){
      this.viewabt = true;
      const body = new FormData();
      body.append('about_brand',this.form.get('company_brief').value);
      this._profileService.updateprofile(this.id,body).subscribe(response=>{
        this.getdata();
      });
  }
  saveabt(){
     this.viewabt = true;
  }

  edithq(){
    this.viewhq = false;
  }

  okhq(){
     this.viewhq = true;

     const body = new FormData();
      body.append('hq_address',this.form.get('address').value);
      body.append('hq_city',this.form.get('city').value);
      body.append('hq_state',this.form.get('state').value);
      body.append('hq_pincode',this.form.get('pincode').value);


     this._profileService.updateprofile(this.id,body).subscribe(response=>{
        this.getdata();
     });
  }
  savehq(){
     this.viewhq = true;
  }
  edittopb(){
    this.viewtopb = false;
  }

  oktopb(){
     this.viewtopb = true;
     var brands = this.form.get('Brand1').value + ',' + this.form.get('Brand2').value + ',' + this.form.get('Brand3').value;
     const body = new FormData();
     body.append('top_retailes',brands)
     this._profileService.updateprofile(this.id,body).subscribe(response=>{
       this.getdata();
     });
  }
  savetopb(){
     this.viewtopb = true;
  }

  segment:any = {
    data: []
  }
  edittops(){
    this.viewtops = false;
    this._profileService.getmastersegment().subscribe(
      response =>{
        this.segment = response;
      }
    )
  }

  oktops(){
     this.viewtops = true;
     var brands = this.form.get('segment1').value + ',' + this.form.get('segment2').value + ',' + this.form.get('segment3').value;
     const body = new FormData();
     body.append('top_segments',brands)
     this._profileService.updateprofile(this.id,body).subscribe(response=>{
       this.getdata();
     });
  }
  savetops(){
     this.viewtops = true;
  }
  category:any = {
    data: []
  }
  edittopc(){
    this.viewtopc = false;
    this._profileService.getmastercategory().subscribe(
      response =>{
        this.category = response;
      });
  }

  oktopc(){
     this.viewtopc = true;
     var brands = this.form.get('categories1').value + ',' + this.form.get('categories2').value + ',' + this.form.get('categories3').value;
     const body = new FormData();
     body.append('top_categories',brands)
     this._profileService.updateprofile(this.id,body).subscribe(response=>{
      this.getdata();
     });
  }
  savetopc(){
     this.viewtopc = true;
  }

  department:any = {
    data: []
  }
  edittopd(){
    this.viewtopd = false;
    this._profileService.getmasterdepartment().subscribe(
      response =>{
        this.department = response;
      });
  }

  oktopd(){
     this.viewtopd = true;
     var brands = this.form.get('department1').value + ',' + this.form.get('department2').value + ',' + this.form.get('department3').value;
     const body = new FormData();
     body.append('top_departments',brands)
     this._profileService.updateprofile(this.id,body).subscribe(response=>{
      this.getdata();
     });
  }
  savepivotal(){
     this.viewpivotal = true;
  }
  editpivotal(){
    this.viewpivotal = false;
  }

  okpivotal(){
     this.viewpivotal = true;
     const body = new FormData();
     body.append('mrp_low',this.form.get('mrp_low').value);
      body.append('mrp_high',this.form.get('mrp_high').value);
     this._profileService.updateprofile(this.id,body).subscribe(response=>{
      this.getdata();
     });
  }
  savetopd(){
     this.viewtopd = true;
  }
  editsocial(){
    this.viewsocial= false;
  }

  oksocial(){
     this.viewsocial = true;
     const body = new FormData();
     body.append('fb_link',this.form.get('facebook').value);
      body.append('google_link',this.form.get('google_plus').value);
      body.append('youtube_link', this.form.get('youtube').value);
      body.append('linkedin_link', this.form.get('linkedin').value);
      body.append('twitter_link', this.form.get('twitter').value);
      body.append('insta_link', this.form.get('instagram').value);
      body.append('website_link', this.form.get('web').value);

     this._profileService.updateprofile(this.id,body).subscribe(response=>{
       this.getdata();
     });
  }
  savesocial(){
     this.viewsocial = true;
  }

  fb(link){
    window.open('https://'+ link, '_blank');
  }


}
