import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector     : 'profile-bank-info',
    templateUrl  : './bank-info.component.html',
    styleUrls    : ['./bank-info.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileBankinfoComponent implements OnInit
{
    banks: any;
    allbanks:any;
    id:any;
    form: FormGroup;
    companyId:any;

    showBankinfoDoneicon = false;
    showBankinfoEditicon = true;


    constructor(
        private _profileService: ProfileService,
        private _formBuilder: FormBuilder,
        public ngxSmartModalService: NgxSmartModalService,
        private router: Router,
        private route: ActivatedRoute
    )
    {
      this.id = this.route.snapshot.paramMap.get('id');
      console.log(this.id);

      const data = JSON.parse(localStorage.getItem('userBrand'));
      this.companyId = data.userData.companyId;

      this.form = _formBuilder.group({
        'bank_name':[''],
      })
    }

    ngOnInit(): void
    {
        this.bankaccs();
        this.companybanks();
    }

    bankaccs(){
      this._profileService.getBankinfo(this.id).subscribe(response => {
        if(response.status){
          this.setdata(response);
        }

      });
    }

    setdata(response){
    this.banks = response.data;
            console.log(response);
    }
    companybanks(){
      this._profileService.getcompanyBankinfo(this.companyId).subscribe(response => {
        if(response.status){
          this.setcompanybanks(response);
        }

      });
    }

    setcompanybanks(response){
        this.allbanks = response.data;
        console.log(response);
    }


      bankaccounts(){
          this._profileService.addbanks(this.id,this.form.get('bank_name').value).subscribe(response=>{
            this.ngxSmartModalService.getModal('addbank').close();
            this.bankaccs();
            this.form.reset()
          });
        }

      removeBank(bankId){
        this._profileService.removeBank(this.id,bankId).subscribe(response=>{

          this.bankaccs();

        });
      }

}
