import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';
import { TreeMapModule } from '@swimlane/ngx-charts';

@Component({
    selector     : 'profile-company',
    templateUrl  : './company.component.html',
    styleUrls    : ['./company.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileCompanyComponent implements OnInit, OnDestroy
{
    company: any;
    id: any;

    companyDetails: FormGroup;
    corporateOfficeDetails: FormGroup;

    showcompanyDoneicon = false;
    showcompanyEditicon = true;

    showcorporateOfficeDoneicon = false;
    showcorporateOfficeEditicon = true;

    // Private


    /**
     * Constructor
     *
     * @param {ProfileService} _profileService
     */
    constructor(
        private _profileService: ProfileService,
        private _formBuilder: FormBuilder,
    )
    {

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {


        this.companyDetails = this._formBuilder.group({
            companyName: [
                {
                    value: this.company.company_details.company_name,
                    disabled: true
                }, Validators.required
            ],
            constitution: [
                {
                    value: this.company.company_details.constitution,
                    disabled: true
                }, Validators.required
            ],
            incomeTax: [
                {
                    value: this.company.company_details.income_tax,
                    disabled: true
                }, Validators.required
            ],
            mobile: [
                {
                    value: this.company.company_details.mobile,
                    disabled: true
                }, Validators.required
            ],
            pincode: [
                {
                    value: this.company.company_details.pincode,
                    disabled: true
                }, Validators.required
            ],
            power_of_attorney: [
                {
                    value: this.company.company_details.power_of_attorney,
                    disabled: true
                }, Validators.required
            ],
            registered_address_proof: [
                {
                    value: this.company.company_details.registered_address_proof,
                    disabled: true
                }, Validators.required
            ],
            retailer_code: [
                {
                    value: this.company.company_details.retailer_code,
                    disabled: true
                }, Validators.required
            ],
            signing_authority_name: [
                {
                    value: this.company.company_details.signing_authority_name,
                    disabled: true
                }, Validators.required
            ],
            tax: [
                {
                    value: this.company.company_details.tax,
                    disabled: true
                }, Validators.required
            ],
            CIN: [
                {
                    value: this.company.company_details.CIN,
                    disabled: true
                }, Validators.required
            ],
            GST: [
                {
                    value: this.company.company_details.GST,
                    disabled: true
                }, Validators.required
            ],
            GST_file: [
                {
                    value: this.company.company_details.GST_file,
                    disabled: true
                }, Validators.required
            ],
            address_proof: [
                {
                    value: this.company.company_details.address_proof,
                    disabled: true
                }, Validators.required
            ],
            attorney: [
                {
                    value: this.company.company_details.attorney,
                    disabled: true
                }, Validators.required
            ],
            certificate: [
                {
                    value: this.company.company_details.certificate,
                    disabled: true
                }, Validators.required
            ],
        });

        this.corporateOfficeDetails = this._formBuilder.group({
            address: [
                {
                    value: this.company.Corporate_office.address,
                    disabled: true
                }, Validators.required
            ],
            city: [
                {
                    value: this.company.Corporate_office.city,
                    disabled: true
                }, Validators.required
            ],
            state: [
                {
                    value: this.company.Corporate_office.state,
                    disabled: true
                }, Validators.required
            ],
            pincode: [
                {
                    value: this.company.Corporate_office.pincode,
                    disabled: true
                }, Validators.required
            ],
            mobileNumber: [
                {
                    value: this.company.Corporate_office.mobile,
                    disabled: true
                }, Validators.required
            ],
            landlineNumber: [
                {
                    value: this.company.Corporate_office.landline,
                    disabled: true
                }, Validators.required
            ],
            addressProof: [
                {
                    value: this.company.Corporate_office.address_proof,
                    disabled: true
                }, Validators.required
            ],
            GSTIN: [
                {
                    value: this.company.Corporate_office.GST,
                    disabled: true
                }, Validators.required
            ],

        });


    }


    companyDetailsEdit(): void {
        this.showcompanyDoneicon = true;
        this.showcompanyEditicon = false;
        this.companyDetails.enable();
    }

    companyDetailsUpdate(): void {
        this.showcompanyDoneicon = false;
        this.showcompanyEditicon = true;
        this.companyDetails.disable();

        console.log(this.companyDetails.value);

        this._profileService.companyDetailsData(this.companyDetails.value, this.id)
            .subscribe(
                response => console.log('Success!', response),
                error => console.error('Error!', error)
            );
    }

    corporateOfficeDetailsEdit(): void {
        this.showcorporateOfficeDoneicon = true;
        this.showcorporateOfficeEditicon = false;
        this.corporateOfficeDetails.enable();
    }
    corporateOfficeDetailsUpdate(): void {
        this.showcorporateOfficeDoneicon = false;
        this.showcorporateOfficeEditicon = true;
        this.corporateOfficeDetails.disable();

        console.log(this.corporateOfficeDetails.value);

        this._profileService.corporateOfficeDetailsData(this.corporateOfficeDetails.value, this.id)
            .subscribe(
                response => console.log('Success!', response),
                error => console.error('Error!', error)
            );
    }


    /**
     * On destroy
     */
    ngOnDestroy(): void
    {

    }
}
