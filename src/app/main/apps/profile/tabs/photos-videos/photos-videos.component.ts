import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ProfileService } from '../../profile.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Lightbox } from 'ngx-lightbox';

@Component({
    selector     : 'profile-photos-videos',
    templateUrl  : './photos-videos.component.html',
    styleUrls    : ['./photos-videos.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfilePhotosVideosComponent implements OnInit, OnDestroy
{
    photosVideos: any;
    id:any;
    lightboxalbum:any =[];

    // Private
    private _unsubscribeAll: Subject<any>;


    constructor(
        private _profileService: ProfileService,
        private router: Router,
        private route: ActivatedRoute,
        private _lightbox: Lightbox
    )
    {
      this.id = this.route.snapshot.paramMap.get('id');
      console.log(this.id);
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this.getgallery()
    }


    getgallery(){
      this._profileService.getprofile(this.id).subscribe(response => {
        if(response.status){
          this.setdata(response);
        }

      });
    }


    setdata(response){
      this.photosVideos = response.data.gallery;
      for(let x of this.photosVideos){
        const album = {
         src: x,
      };
            this.lightboxalbum.push(album);
      }

    }

    uploadgalimage(event){

      const body = new FormData();
      var file1 = [];
      for(let i = 0 ; i< event.target.files.length;i++){
        body.append('gallery'+i, event.target.files[i]);
      //  body.append('file', event.target.files[i]);
      }
      this._profileService.uploadgal(this.id,body).subscribe(response => {

        var showElement = true;
setTimeout(() => {
  this.getgallery();
    var showElement = false;
}, 4000);
      });
    }

    deleteimage(index){
      this._profileService.deleteimage(index,this.id).subscribe(response=>{
      this.getgallery();
    });



    }

    open(index) {
      console.log(this.photosVideos);
  this._lightbox.open(this.lightboxalbum, index,{resizeDuration:0.8,fadeDuration:1,enableTransition:false});
}

    close() {
      this._lightbox.close();
    }


    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
