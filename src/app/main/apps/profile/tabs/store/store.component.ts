import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';

@Component({
    selector     : 'profile-store',
    templateUrl  : './store.component.html',
    styleUrls    : ['./store.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileStoreComponent implements OnInit, OnDestroy
{
    store: any;
    id: any;
    form: FormGroup;
    storeDetails: FormGroup;

    showStoreDoneicon = false;
    showStoreEditicon = true;


    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProfileService} _profileService
     */
    constructor(
        private _profileService: ProfileService,
        private _formBuilder: FormBuilder,
        public ngxSmartModalService: NgxSmartModalService
    )
    {
        this._unsubscribeAll = new Subject();

        const data = JSON.parse(localStorage.getItem('UserDataR'));
        this.id = data.retailerId;

        this.form = _formBuilder.group({
          'store_name':[''],
          'email':[''],
          'mobile':[''],
          'landline':[''],
          'address':[''],
          'city':[''],
          'state':[''],
          'pincode':[''],
          'GST':['']
        })

    }

    ngOnInit(): void
    {
      this.getstores();
    }

    getstores(){
      this._profileService.getstore(this.id).subscribe(response=>{
        console.log(response);
        this.store = response;
      })
    }

    addstores(){
      const data = this.form.value;
      console.log(this.id);
      this._profileService.addstore(data,this.id).subscribe(response=>{

        this.ngxSmartModalService.getModal('addstore').close();
        this.getstores();
        this.form.reset();
      });
    }



    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
