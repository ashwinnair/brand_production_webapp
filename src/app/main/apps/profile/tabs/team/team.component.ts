import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';
import { TreeMapModule } from '@swimlane/ngx-charts';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector     : 'profile-team',
    templateUrl  : './team.component.html',
    styleUrls    : ['./team.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileTeamComponent implements OnInit
{
    team: any;
    id: any;
    addteam:FormGroup;
    companyId:any;
    authteam:any;
    companyteam:any;


    constructor(
        private _profileService: ProfileService,
        private fb: FormBuilder,
        public ngxSmartModalService: NgxSmartModalService,
        private router: Router,
        private route: ActivatedRoute
    )
    {

        this.id = this.route.snapshot.paramMap.get('id');
        console.log(this.id);

        const data = JSON.parse(localStorage.getItem('userBrand'));
        this.companyId = data.userData.companyId;

        this.addteam = fb.group({
          'firstname':['']
        })

    }

    ngOnInit(): void
    {
      this.getauthteam();
      this.getcompanyteam();
    }

    getcompanyteam(){
      this._profileService.getcompanyTeam(this.companyId).subscribe(response=>{

          if(response.status){
            this.setcompanyteamdata(response);
          }

      })
    }

    setcompanyteamdata(response){
        this.companyteam = response.data;
    }

    addteamtobrand(){
      this._profileService.addTeamtoBrand(this.id, this.addteam.get('firstname').value).subscribe(response =>{
        this.getauthteam();
        this.addteam.reset();
        this.ngxSmartModalService.getModal('addopsteam').close();
      })
    }

    getauthteam(){
      this._profileService.getauthteam(this.id).subscribe(response=>{
        if(response.status){
          this.setdata(response);
        }

      });
    }

    setdata(response){
        this.authteam = response.data;
    }


    deleteopsteam(teamId){

      this._profileService.removeTeamFromBrand(this.id, teamId).subscribe(response =>{
        this.getauthteam();
      })

    }





}
