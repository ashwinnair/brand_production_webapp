import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from '../../profile.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector     : 'profile-warehouse',
    templateUrl  : './warehouse.component.html',
    styleUrls    : ['./warehouse.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileWarehouseComponent implements OnInit
{
    warehouses: any;
    id: any;
    form: FormGroup;
    companyId:any;
    companywarehouse:any;


    private _unsubscribeAll: Subject<any>;

    constructor(
        private _profileService: ProfileService,
        private _formBuilder: FormBuilder,
        public ngxSmartModalService: NgxSmartModalService,
        private router: Router,
        private route: ActivatedRoute
    )
    {
      this.id = this.route.snapshot.paramMap.get('id');
      const data = JSON.parse(localStorage.getItem('userBrand'));
      this.companyId = data.userData.companyId;

      this.form = _formBuilder.group({
        'warehouse_name':['']
      });


        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
      this.getwarehouse();
      this.getcompanywarehouse();
    }

    getwarehouse(){
      this._profileService.getwarehouse(this.id).subscribe(response =>{
        if(response.status){
          this.setBrandwarehouse(response);
        }

      });
    }

    setBrandwarehouse(response){

      this.warehouses=response.data;
      console.log(this.warehouses);
    }

    getcompanywarehouse(){
      this._profileService.getcompanyWarehouse(this.companyId).subscribe(response=>{
          if(response.status){
            this.setcompanyteamdata(response);
          }

      })
    }

    setcompanyteamdata(response){
        this.companywarehouse = response.data;
        console.log(this.companywarehouse);
    }

    addwarehouses(){
      this._profileService.addwarehousetobrand(this.id, this.form.get('warehouse_name').value).subscribe(response =>{
        this.getwarehouse();
        this.form.reset();
        this.ngxSmartModalService.getModal('addDeliverloc').close();
      })
    }

    removeWarehouse(warehouseId)
    {
      this._profileService.removeWarehouseFromBrand(this.id, warehouseId).subscribe(response =>{
        this.getwarehouse();

      })
    }




}
